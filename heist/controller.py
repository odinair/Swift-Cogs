import asyncio
from datetime import datetime, timedelta
from math import ceil
from random import random
from typing import Dict, Optional

import discord
from redbot.core import bank

from . import errors
from .shared import translate, HeistConfig


class HeistController:
    def __init__(self, guild: discord.Guild):
        self.guild = guild
        self.users: Dict[discord.Member, int] = {}
        self.last_heist: Optional[datetime] = ...

    async def cooldown(self) -> Optional[timedelta]:
        if self.last_heist is ...:
            last = await HeistConfig.last_heist(self.guild)
            if last is None:
                self.last_heist = None
            else:
                # For whatever dumb as fuck reason, datetime doesn't return what you'd probably
                # expect from utcfromtimestamp compared to utcnow. So, of course - we get
                # to do this:
                difference = (
                    datetime.utcfromtimestamp(datetime.utcnow().timestamp()) - datetime.utcnow()
                )
                # ... then, use that timedelta to get the actual datetime of the last heist.
                # datetime is a wonderful library.
                self.last_heist = datetime.utcfromtimestamp(last) - difference
        if self.last_heist is None:
            return None

        return (
            self.last_heist + timedelta(seconds=await HeistConfig.cooldown(self.guild))
        ) - datetime.utcnow()

    async def join(self, user: discord.Member, amount: int):
        cooldown = await self.cooldown()
        if cooldown and cooldown.total_seconds() > 0:
            raise errors.HeistOnCooldown(cooldown)
        if user in self.users:
            raise errors.AlreadyInHeist()
        if not await bank.can_spend(user, amount):
            raise errors.NotEnoughCredits()

        await bank.withdraw_credits(user, amount)
        self.users[user] = amount

    def wrap(self, loop):
        return lambda *a, **k: loop.create_task(self(*a, **k))

    async def __call__(self, channel: discord.TextChannel):
        self.last_heist = datetime.utcnow()
        await HeistConfig.last_heist(self.guild).set(self.last_heist.timestamp())
        chance = await HeistConfig.chance(self.guild)
        multiplier = await HeistConfig.multiplier(self.guild)

        entered = self.users
        self.users = {}
        survivors = {}
        for user, bid in entered.items():
            if random() < chance:
                survivors[user] = ceil(bid * multiplier)

        for survivor, bid in survivors.items():
            await bank.deposit_credits(survivor, bid)
        entered, survived = len(entered), len(survivors)

        await channel.send(
            translate(
                "outcomes.survivors",
                survivors="none" if survived == 0 else "few" if survived != entered else "all",
            )
        )
        if survived:
            await asyncio.sleep(1.5)
            await channel.send(
                translate(
                    "outcomes.survivor_list",
                    users=", ".join([f"{k.mention} ({v})" for k, v in survivors.items()]),
                )
            )
