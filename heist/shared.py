from __future__ import annotations

import logging
from typing import Callable, Type, Tuple

import discord
from redbot.core import Config, bank

from swift_i18n.red import Translator

translate = Translator(__file__)
log = logging.getLogger("red.swift_cogs.heist")


class _ConfValue:
    def __init__(self, guild: discord.Guild, cfg: Type[HeistConfig], key: Tuple[str], kwargs: dict):
        self._key = key
        self._cfg = cfg
        self._guild = guild
        self.kwargs = kwargs

    def __await__(self):
        return self._cfg.get(self._guild, *self._key, **self.kwargs).__await__()

    async def set(self, value):
        kwargs = self.kwargs.copy()
        kwargs["value"] = value
        await self._cfg.set(self._guild, *self._key, **kwargs)


def _wrapper(*key: str, **kwargs) -> Callable[[discord.Guild], _ConfValue]:
    # noinspection PyDecorator
    @classmethod
    def actual_method(cls, guild: discord.Guild):
        return _ConfValue(guild, cls, key, kwargs)

    # noinspection PyTypeChecker
    return actual_method


class HeistConfig:
    try:
        config = Config.get_conf(
            cog_instance=None, cog_name="Heist", identifier=436343824, force_registration=True
        )
        _defaults = {
            "cooldown": 30 * 60,
            "delay": 120.0,
            "multiplier": 2.0,
            "chance": 0.5,
            "min_bet": 1,
        }
        _guild_defaults = {"last_heist": None, **_defaults}
        config.register_guild(**_guild_defaults)
        config.register_global(**_defaults)
    except RuntimeError:
        config = None

    @classmethod
    async def get(cls, guild: discord.Guild, *key: str, default=..., glbl: bool = True):
        if glbl and await bank.is_global():
            return await cls.config.get_raw(*key, default=default)
        else:
            return await cls.config.guild(guild).get_raw(*key, default=default)

    @classmethod
    async def set(cls, guild: discord.Guild, *key: str, value, glbl: bool = True):
        if glbl and await bank.is_global():
            await cls.config.set_raw(*key, value=value)
        else:
            await cls.config.guild(guild).set_raw(*key, value=value)

    cooldown = _wrapper("cooldown")
    delay = _wrapper("delay")
    multiplier = _wrapper("multiplier")
    chance = _wrapper("chance")
    min_bet = _wrapper("min_bet")
    last_heist = _wrapper("last_heist", glbl=False)
