from typing import Dict

import discord
from redbot.cogs.bank import is_owner_if_bank_global
from redbot.core import commands, bank, checks
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import warning

from heist import errors
from heist.controller import HeistController
from heist.shared import translate, HeistConfig
from swift_i18n import Humanize


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


@translate.cog("help.root")
class Heist(commands.Cog):
    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.heists: Dict[discord.Guild, HeistController] = {}

    async def red_delete_data_for_user(self, **_):
        pass  # nothing to delete

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    @translate.command("help.command.root")
    async def heist(self, ctx: commands.Context, bet: int):
        min_bet: int = await HeistConfig.min_bet(ctx.guild)
        if bet < min_bet:
            await ctx.send(
                warning(
                    translate(
                        "bet_too_low",
                        min_bet=min_bet,
                        currency_name=await bank.get_currency_name(ctx.guild),
                    )
                )
            )
            return

        if ctx.guild not in self.heists:
            self.heists[ctx.guild] = HeistController(guild=ctx.guild)
        heist = self.heists[ctx.guild]

        try:
            await heist.join(ctx.author, bet)
        except errors.HeistOnCooldown as e:
            await ctx.send(warning(translate("on_cooldown", delta=Humanize(e.args[0]))))
        except errors.AlreadyInHeist:
            await ctx.send(warning(translate("in_heist")))
        except errors.NotEnoughCredits:
            await ctx.send(
                warning(
                    translate(
                        "insufficent_currency",
                        currency_name=await bank.get_currency_name(ctx.guild),
                    )
                )
            )
        else:
            await ctx.tick()
            if len(heist.users) == 1:
                await ctx.send(translate("start", user=ctx.author.mention, prefix=ctx.clean_prefix))
                # you'd think this would be able to handle async methods, but surprise! it doesn't.
                self.bot.loop.call_later(
                    await HeistConfig.delay(ctx.guild), heist.wrap(self.bot.loop), ctx.channel
                )

    @heist.group(name="cooldown", invoke_without_command=True)
    @is_owner_if_bank_global()
    @checks.admin()
    @translate.command("help.command.cooldown")
    async def heist_cooldown(self, ctx: commands.Context, *, cooldown: commands.TimedeltaConverter):
        if not cooldown:
            raise commands.BadArgument()
        await HeistConfig.cooldown(ctx.guild).set(cooldown.total_seconds())
        await ctx.send(tick(translate("cooldown_set", delta=Humanize(cooldown))))

    @heist.command(name="warmup", aliases=["delay"])
    @is_owner_if_bank_global()
    @checks.admin()
    @translate.command("help.command.delay")
    async def heist_delay(
        self,
        ctx: commands.Context,
        *,
        delay: commands.get_timedelta_converter(default_unit="seconds"),
    ):
        if not delay:
            raise commands.BadArgument()
        await HeistConfig.delay(ctx.guild).set(delay.total_seconds())
        await ctx.send(tick(translate("delay_set", delta=Humanize(delay))))

    @heist.command(name="chance")
    @is_owner_if_bank_global()
    @checks.admin()
    @translate.command("help.command.chance")
    async def heist_chance(self, ctx: commands.Context, *, chance: int):
        if 0 > chance or chance > 100:
            await ctx.send(translate("chance_limit"))
            return

        await HeistConfig.chance(ctx.guild).set(chance / 100)
        await ctx.send(tick(translate("chance_set", chance=chance)))

    @heist.command(name="multiplier")
    @is_owner_if_bank_global()
    @checks.admin()
    @translate.command("help.command.multiplier")
    async def heist_multiplier(self, ctx: commands.Context, *, multiplier: float):
        if multiplier < 1:
            await ctx.send(translate("multiplier_under"))
            return

        await HeistConfig.multiplier(ctx.guild).set(multiplier)
        await ctx.send(tick(translate("multiplier_set", multiplier=multiplier)))

    @heist.command(name="minbet")
    @is_owner_if_bank_global()
    @checks.admin()
    @translate.command("help.command.min_bet")
    async def heist_min_bet(self, ctx: commands.Context, *, min_bet: int):
        if min_bet < 1:
            await ctx.send(translate("min_bet_under"))
            return

        await HeistConfig.min_bet(ctx.guild).set(min_bet)
        await ctx.send(tick(translate("min_bet_set", min_bet=min_bet)))

    # debug commands

    @heist_cooldown.command(hidden=True, name="resetcd")
    @checks.is_owner()
    async def reset_heist_cooldown(self, ctx: commands.Context):
        await HeistConfig.last_heist(ctx.guild).set(None)
        if ctx.guild in self.heists:
            self.heists[ctx.guild].last_heist = None
        await ctx.tick()
