"""Core loading and unloading logic for toolsets

This file is largely responsible for the bulk of how MiscTools nests its commands under
the main cog class, despite each command being in completely different classes with
no clear connections.

If you're here to try and figure out how this is working, you'll also want to look at
the `commands.py` and `toolset.py` files.
"""

from typing import Type, Union

import discord
from redbot.core.bot import Red

from misctools.shared import _ as T_
from misctools.shared import config, log
from misctools.tools import toolsets
from misctools.toolset import Toolset
from red_icu.util import LazyStr

__all__ = ("UnknownToolset", "NotLoaded", "AlreadyLoaded", "InternalLoadError", "LoadLogic")


# noinspection PyPep8Naming
def _T(s):
    return s


_ = _T


class UnknownToolset(KeyError):
    pass


class NotLoaded(KeyError):
    pass


class AlreadyLoaded(KeyError):
    pass


class InternalLoadError(Exception):
    def __init__(self, translated: Union[str, LazyStr], untranslated: str, **kwargs):
        super().__init__(untranslated)
        if isinstance(translated, str):
            self.translated = T_.lazy(translated, **kwargs)
        else:
            self.translated = translated
        self.untranslated = untranslated


class LoadLogic:
    def __init__(self, bot: Red, cog):
        self.bot = bot
        self.cog = cog
        self.loaded = []

    async def bootstrap(self):
        to_load = await config.toolsets()
        loaded = []
        for name in to_load:
            try:
                await self.load(name)
            except AlreadyLoaded:
                log.warning("Toolset %r is referenced more than once in startup config", name)
            except UnknownToolset:
                log.warning(
                    "Failed to find a toolset with the name %r; removing from"
                    " load config and skipping",
                    name,
                )
                async with config.toolsets() as configured:
                    configured.remove(name)
            except InternalLoadError as e:
                log.error("Could not load toolset %r: %s", name, e.untranslated)
            # Treat any other exception as a fatal exception and bail
            except Exception as e:
                self.cog_unload()
                raise RuntimeError(f"Failed to load toolset {name!r}") from e
            else:
                loaded.append(name.lower())
        if loaded:
            log.info("Loaded toolsets: %s", ", ".join(loaded))

    def cog_unload(self):
        log.debug("MiscTools is being unloaded, tearing down loaded toolsets")
        for toolset in self.loaded.copy():
            try:
                self.unload(toolset)
            except Exception as e:  # pylint:disable=broad-except
                log.exception("Failed to unload toolset %r", toolset.__class__.__name__, exc_info=e)

    def is_loaded(self, toolset: Union[Toolset, Type[Toolset], str]) -> bool:
        # an instantiated Toolset being passed is possible during load if an error occurs
        if isinstance(toolset, Toolset) and type(toolset) is not type:
            toolset = type(toolset)
        if isinstance(toolset, str):
            toolset = self.find(toolset)
        return any([type(x) is toolset for x in self.loaded])

    @staticmethod
    def find(toolset: str):
        try:
            return next(x for x in toolsets if x.__name__.lower() == toolset.lower())
        except StopIteration:
            raise UnknownToolset(toolset)

    @staticmethod
    def find_replaced_by(toolset: str):
        """Currently unused."""
        return next(
            (
                x
                for x in toolsets
                if x.__replaces__ and any(z.lower() == toolset.lower() for z in x.__replaces__)
            ),
            None,
        )

    async def load(self, toolset: Union[Type[Toolset], str]):
        """Add a toolset's commands to the bot"""

        if isinstance(toolset, str):
            toolset = self.find(toolset)
        if self.is_loaded(toolset):
            raise AlreadyLoaded(toolset)

        # Better feedback for command conflicts instead of just outright failing to load
        for command in toolset.__commands__.values():
            if command.overwrite_existing:
                log.debug("command %r overwrites existing commands", command.qualified_name)
                continue
            if self.bot.get_command(command.qualified_name):
                conflicts_with = self.bot.get_command(command.qualified_name).cog
                conflicts_with = getattr(conflicts_with, "qualified_name", conflicts_with)
                raise InternalLoadError(
                    _("The command `{command}` conflicts with an existing command from {cog}"),
                    f"Command {command.qualified_name!r} conflicts with a command with the"
                    f" same name from cog {conflicts_with!r}",
                    command=command.qualified_name,
                    cog=conflicts_with,
                )

        toolset = toolset(self.bot)
        try:
            if toolset.__load_methods__:
                for method in toolset.__load_methods__:
                    coro = discord.utils.maybe_coroutine(method.func, toolset)
                    if method.blocking:
                        await coro
                    else:
                        self.bot.loop.create_task(coro)

            for command in toolset.__commands__.values():
                log.debug("Setting up command %r", command.qualified_name)
                # pylint:disable=protected-access
                command._actual_self = toolset
                command._appear_from = self.cog
                # pylint:enable=protected-access
                if not command.parent:
                    if command.overwrite_existing:
                        if replacing := self.bot.get_command(command.qualified_name):
                            log.debug("replacing command %r", replacing.qualified_name)
                            command._overwritten = replacing
                            self.bot.remove_command(replacing.qualified_name)
                    self.bot.add_command(command)
        except Exception:
            if self.is_loaded(toolset):
                self.unload(toolset)
            raise

        self.loaded.append(toolset)
        self.update_commands()

    def unload(self, toolset: Union[Type[Toolset], str, Toolset]):
        """Remove a toolset's commands from the bot"""

        toolset = (
            (type(toolset) if type(toolset) is not type else toolset)
            if isinstance(toolset, Toolset)
            else self.find(toolset)
        )
        try:
            toolset = next(x for x in self.loaded if type(x) is toolset)
        except StopIteration:
            raise NotLoaded(toolset if isinstance(toolset, str) else toolset.__name__)

        for command in list(self.bot.all_commands.values()):
            if (
                command.cog is self
                and (not command.parent or isinstance(command.parent, Red))
                and any(
                    x.qualified_name == command.qualified_name
                    for x in toolset.__commands__.values()
                )
            ):
                self.bot.remove_command(command.qualified_name)

        # This is necessary for cog unload.
        for command in toolset.__commands__.values():
            # noinspection PyProtectedMember
            if replace_with := command._overwritten:
                # noinspection PyUnresolvedReferences
                log.debug("loading back replaced command %r", replace_with.qualified_name)
                # noinspection PyTypeChecker
                self.bot.add_command(replace_with)
                command._overwritten = None

        toolset.toolset_cleanup()
        self.loaded.remove(toolset)
        self.update_commands()

    def update_commands(self):
        """Update the main cog's commands mapping to include loaded toolset commands"""
        # This might be able to be redone to use ephemeral commands instead of
        # dynamically updating the cog's commands mapping tuple, although I'm
        # unsure if that'd possibly cause issues with how this cog is expected to work.

        from misctools.commands import _CommandMixin

        cmds = [x for x in self.cog.__cog_commands__ if not isinstance(x, _CommandMixin)]
        for toolset in self.loaded:
            cmds.extend(toolset.__commands__.values())
        self.cog.__cog_commands__ = (*cmds,)
