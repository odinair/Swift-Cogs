from datetime import datetime
from typing import Union

import discord
from babel.lists import format_list
from redbot.core import checks
from redbot.core.i18n import get_babel_regional_format
from redbot.core.utils.common_filters import filter_invites

from misctools import commands
from misctools.shared import _
from misctools.toolset import Toolset
from red_icu import Humanize, cog_i18n

STATUS = {
    discord.Status.online: _("\N{LARGE GREEN CIRCLE} Online"),
    discord.Status.dnd: _("\N{LARGE RED CIRCLE} Do Not Disturb"),
    discord.Status.idle: _("\N{LARGE YELLOW CIRCLE} Idle"),
    discord.Status.offline: _("\N{SLEEPING SYMBOL} Offline"),
}
ACTIVITY = {
    discord.ActivityType.playing: _("\N{VIDEO GAME} Playing **{name}**"),
    discord.ActivityType.streaming: _("\N{VIDEO CAMERA} Streaming [**{name}**]({url})"),
    discord.ActivityType.watching: _("\N{FILM PROJECTOR} Watching **{name}**"),
    discord.ActivityType.listening: _("\N{MUSICAL NOTE} Listening to **{name}**"),
    discord.ActivityType.competing: _("\N{CROSSED SWORDS} Competing in **{name}**"),
}
SPOTIFY = _("\N{MUSICAL NOTE} Listening to **{title}** by **{artists}** on **Spotify**")
UNKNOWN_STATUS = _("\N{BLACK QUESTION MARK ORNAMENT} Unknown status")
UNKNOWN_ACTIVITY = _("\N{BLACK QUESTION MARK ORNAMENT} Unknown activity")


@cog_i18n(_)
class UInfo(Toolset, name=_("User Info")):
    """Fancier variation on `[p]userinfo`"""

    async def get_names_and_nicks(self, member: Union[discord.Member, discord.User]):
        try:
            mod = self.bot.cogs["Mod"]
        except KeyError:
            return [], []

        names = await mod.config.user(member).past_names()
        nicks = (
            await mod.config.member(member).past_nicks()
            if isinstance(member, discord.Member)
            else []
        )

        if names:
            names = [filter_invites(name) for name in names if name]
        if nicks:
            nicks = [filter_invites(nick) for nick in nicks if nick]
        return names, nicks

    async def build_description(self, member: discord.Member) -> str:
        desc = [str(STATUS.get(member.status, UNKNOWN_STATUS))]

        if member.nick:
            desc.append(_("\N{LABEL} Nicknamed as {nick}").format(nick=member.nick))

        for activity in member.activities:
            # Ignore custom statuses
            if activity.type == discord.ActivityType.custom:
                continue
            desc.append(
                (
                    ACTIVITY.get(activity.type, UNKNOWN_ACTIVITY)
                    if not isinstance(activity, discord.Spotify)
                    else SPOTIFY
                ).format(
                    artists=Humanize(getattr(member.activity, "artists", [])),
                    title=getattr(member.activity, "title", "???"),
                    name=member.activity.name,
                    url=getattr(member.activity, "url", None),
                )
            )

        if member.is_on_mobile():
            desc.append(_("\N{MOBILE PHONE} Active on Mobile").format())

        if member.voice:
            desc.append(
                _("\N{SPEAKER} In voice channel {channel}").format(
                    channel=member.voice.channel.mention
                )
            )

        if member.bot:
            desc.append(_("\N{ROBOT FACE} Bot").format())
        else:
            if await self.bot.is_owner(member):
                desc.append(_("\N{HAMMER AND WRENCH} **Bot Owner**").format())

            if member.guild.owner.id == member.id:
                desc.append(_("\N{KEY} Server Owner").format())
            elif await self.bot.is_admin(member):
                desc.append(_("\N{HAMMER AND WRENCH} Server Administrator").format())
            elif await self.bot.is_mod(member):
                desc.append(_("\N{SHIELD} Server Moderator").format())

        if member.premium_since:
            desc.append(
                _(
                    "\N{WHITE MEDIUM STAR} Nitro boosting this server for {delta} since {date}"
                ).format(
                    delta=Humanize(member.premium_since - datetime.utcnow()),
                    date=Humanize(member.premium_since.date()),
                )
            )

        return "\n".join(desc)

    @commands.command(aliases=["uinfo", "whois"])
    @commands.guild_only()
    @checks.bot_has_permissions(embed_links=True)
    async def user(self, ctx: commands.Context, *, member: discord.Member = None):
        """Get information on a given user"""
        member = member or ctx.author
        embed = discord.Embed(
            title=str(member),
            colour=member.colour if member.colour.value != 0 else discord.Embed.Empty,
            description=await self.build_description(member),
        ).set_thumbnail(url=str(member.avatar_url_as(static_format="png")))

        now = datetime.utcnow()
        member_n = sorted(member.guild.members, key=lambda m: m.joined_at or now).index(member) + 1
        embed.set_footer(text=_("Member #{n} | User ID: {id}").format(n=member_n, id=member.id))

        embed.add_field(
            name=_("Account Age").format(),
            value="\n".join(
                [
                    _("\N{SPIRAL CALENDAR PAD} Joined Discord {delta} on {absolute}").format(
                        delta=Humanize(
                            member.created_at - ctx.message.created_at, add_direction=True
                        ),
                        absolute=Humanize(member.created_at.date()),
                    ),
                    _("\N{CLOCK FACE TWO OCLOCK} Joined server {delta} on {absolute}").format(
                        delta=Humanize(
                            member.joined_at - ctx.message.created_at, add_direction=True
                        ),
                        absolute=Humanize(member.joined_at.date()),
                    ),
                ]
            ),
            inline=False,
        )

        roles = list(reversed([x.mention for x in member.roles if not x.is_default()]))
        cap = 40
        if len(roles) > cap:
            roles = [
                *roles[:cap],
                _("{num, plural, one {# more role} other {# more roles}}").format(
                    num=len(roles) - cap
                ),
            ]
        if roles:
            embed.add_field(
                name=_("Roles").format(),
                value=format_list(roles, locale=get_babel_regional_format()),
                inline=False,
            )

        names, nicks = await self.get_names_and_nicks(member)
        if names:
            embed.add_field(name=_("Past Names").format(), value=", ".join(names), inline=False)
        if nicks:
            embed.add_field(name=_("Past Nicknames").format(), value=", ".join(nicks), inline=False)

        await ctx.send(embed=embed)
