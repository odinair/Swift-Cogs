import copy
import unicodedata
from datetime import datetime
from inspect import getcomments, getsource
from textwrap import dedent
from typing import Union

import discord
from babel.dates import format_datetime, format_timedelta
from redbot.core import checks
from redbot.core.i18n import get_babel_regional_format
from redbot.core.utils import deduplicate_iterables
from redbot.core.utils.chat_formatting import pagify, warning
from tzlocal import get_localzone

from misctools import commands
from misctools.shared import _, log
from misctools.toolset import Toolset
from misctools.utils import flatten_list
from red_icu import Humanize, cog_i18n


@cog_i18n(_)
class Dev(Toolset, name=_("Dev Tools")):
    """Cog development utilities"""

    # TODO this command will conflict with Core if red#3999 is merged
    #
    # This command is taken from jishaku:
    # https://github.com/Gorialis/jishaku/blob/fc7c479/jishaku/cog.py#L346-L359
    # noinspection PyProtectedMember
    @commands.command()
    @checks.is_owner()
    @commands.is_dev()
    async def sudo(self, ctx: commands.Context, *, command: str):
        """Forcibly run a command, bypassing checks and cooldowns

        This command is only available if you started your bot
        with either the `--dev` or `--debug` flags.

        Commands with support for it also bypass yes/no checks
        when this command is used to invoke them.
        """
        alt_message: discord.Message = copy.copy(ctx.message)
        # pylint:disable=protected-access
        alt_message._update({"content": ctx.prefix + command})
        alt_ctx = await ctx.bot.get_context(alt_message, cls=type(ctx))
        # pylint:enable=protected-access

        if alt_ctx.command is None:
            return await ctx.send(_("That command doesn't exist."))

        # bypass yes/no checks for commands that support it
        alt_ctx.assume_yes = True

        log.info("%s (%s) used sudo to invoke command: %r", ctx.author, ctx.author.id, command)
        return await alt_ctx.command.reinvoke(alt_ctx)

    @commands.command()
    async def rtfs(self, ctx: commands.Context, *, command_name: str):
        """Get the source code for a command"""
        command = self.bot.get_command(command_name)
        if command is None:
            await ctx.send(
                warning(
                    _("No such command named `{command}` exists").format(
                        command=command_name.replace("`", "\\`")
                    )
                )
            )
            return

        try:
            callback = command.callback
            # Honestly, I'm not even sure this is needed; can't hurt to keep it anyways.
            while hasattr(callback, "__wrapped__"):
                callback = callback.__wrapped__

            source = dedent(getsource(callback))
            if comments := getcomments(callback):
                source = dedent(comments) + source

            source = pagify(dedent(source).replace("`", "\N{ZERO WIDTH JOINER}`"))
        except OSError:
            # The most likely way we'd encounter this error is if the source file was deleted,
            # or if the command was created with a cog like instantcmd or in an eval.
            await ctx.send(
                warning(
                    _(
                        "I failed to retrieve the source code for that command. Either the"
                        " source file for the command was deleted since being loaded,"
                        " or it was created in memory."
                    ).format()
                )
            )
        else:
            await ctx.send_interactive(source, box_lang="py")

    @commands.command(usage="<characters...>")
    async def charinfo(self, ctx: commands.Context, *characters: Union[discord.PartialEmoji, str]):
        """Get the unicode name for one or more characters

        This command also supports Discord emojis, and returns their ID instead of a unicode name.

        Up to 25 characters or emojis may be given with any one command invocation.
        """
        if not characters:
            await ctx.send_help()
            return

        characters = deduplicate_iterables(
            flatten_list(
                [
                    x if isinstance(x, discord.PartialEmoji) else [z for z in x if z != " "]
                    for x in characters
                ]
            )
        )

        if len(characters) > 25:
            await ctx.send_help()
            return

        # the following is largely ripped from R Danny, with some changes made:
        # https://github.com/Rapptz/RoboDanny/blob/ee101d1/cogs/meta.py#L209-L223

        def to_str(char: Union[commands.PartialEmojiConverter, str]):
            if isinstance(char, discord.PartialEmoji):
                return f"{char} \N{EM DASH} `{char.id}`"
            else:
                digit = ord(char)
                name = unicodedata.name(char, _("Name not found"))
                return f"{char} \N{EM DASH} `{name!s}` (`\\U{digit:>08}`)"

        await ctx.send("\n".join(map(to_str, characters)))

    @commands.group(aliases=["snowflaketime", "createdat"], invoke_without_command=True)
    async def snowflake(self, ctx: commands.Context, *snowflakes: int):
        """Find out when a given ID was created"""
        if not snowflakes:
            await ctx.send_help()
            return

        await ctx.send_interactive(
            pagify(
                "\n".join(
                    [
                        "{}: {} \N{EM DASH} `{}`".format(
                            snowflake,
                            format_timedelta(
                                discord.utils.snowflake_time(snowflake) - datetime.utcnow(),
                                add_direction=True,
                                locale=get_babel_regional_format(),
                            ),
                            format_datetime(
                                discord.utils.snowflake_time(snowflake),
                                locale=get_babel_regional_format(),
                                tzinfo=get_localzone(),
                            ),
                        )
                        for snowflake in snowflakes
                    ]
                )
            )
        )

    @snowflake.command(name="delta")
    async def snowflake_delta(self, ctx: commands.Context, starting: int, ending: int):
        """Get the time between the creation two IDs"""
        starting, ending = (
            discord.utils.snowflake_time(starting),
            discord.utils.snowflake_time(ending),
        )
        if ending > starting:
            await ctx.send(_("The ending ID was created before the starting ID").format())
            return
        now = datetime.utcnow()

        await ctx.send(
            _(
                "**Starting snowflake:** {start_delta} \N{EM DASH} `{start_date}`\n"
                "**Ending snowflake:** {end_delta} \N{EM DASH} `{end_date}`\n\n"
                "**Time difference:** {difference}"
            ).format(
                start_delta=Humanize(starting - now, add_direction=True),
                start_date=Humanize(starting),
                end_delta=Humanize(ending - now, add_direction=True),
                end_date=Humanize(ending),
                difference=Humanize(ending - starting),
            )
        )
