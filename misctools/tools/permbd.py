from math import ceil
from typing import Dict, Iterable, List, Optional, Sequence, Union

import discord
from redbot.vendored.discord.ext.menus import ListPageSource as _ListPageSource
from redbot.vendored.discord.ext.menus import MenuPages

from misctools import commands
from misctools.shared import _
from misctools.toolset import Toolset
from red_icu import Humanize, cog_i18n

perm_channel_types = {
    discord.TextChannel: (
        # read_messages isn't included here as voice channels use it as the view channel permission
        "add_reactions",
        "send_messages",
        "send_tts_messages",
        "manage_messages",
        "embed_links",
        "attach_files",
        "read_message_history",
        "mention_everyone",
        "external_emojis",
    ),
    discord.VoiceChannel: (
        "connect",
        "speak",
        "mute_members",
        "deafen_members",
        "move_members",
        "use_voice_activation",
        "priority_speaker",
        "stream",
    ),
}
permission_translations = {
    "create_instant_invite": _("Create Instant Invite"),
    "kick_members": _("Kick Members"),
    "ban_members": _("Ban Members"),
    "administrator": _("Administrator"),
    "manage_channels": _("Manage Channels"),
    "manage_guild": _("Manage Server"),
    "add_reactions": _("Add Reactions"),
    "view_audit_log": _("View Audit Log"),
    "priority_speaker": _("Priority Speaker"),
    "stream": _("Stream"),
    "read_messages": _("Read Messages"),
    "send_messages": _("Send Messages"),
    "send_tts_messages": _("Send TTS Messages"),
    "manage_messages": _("Manage Messages"),
    "embed_links": _("Embed Links"),
    "attach_files": _("Attach Files"),
    "read_message_history": _("Read Message History"),
    "mention_everyone": _("Mention Everyone"),
    "external_emojis": _("Use External Emojis"),
    "view_guild_insights": _("View Server Insights"),
    "connect": _("Connect"),
    "speak": _("Speak"),
    "mute_members": _("Mute Members"),
    "deafen_members": _("Deafen Members"),
    "move_members": _("Move Members"),
    "use_voice_activation": _("Use Voice Activation"),
    "change_nickname": _("Change Nickname"),
    "manage_nicknames": _("Manage Nicknames"),
    "manage_roles": _("Manage Roles"),
    "manage_webhooks": _("Manage Webhooks"),
    "manage_emojis": _("Manage Emojis"),
}


def format_permission(perm: str):
    return str(
        permission_translations.get(perm, perm.replace("guild", "server").replace("_", " ").title())
    )


class ListPageSource(_ListPageSource):
    def format_page(self, menu, page):
        return page


def chunks(seq: Sequence, chunk_every: int) -> Iterable[Sequence]:
    for i in range(0, len(seq), chunk_every):
        yield seq[i : i + chunk_every]


def is_applicable(perm: str, channel: discord.abc.GuildChannel):
    try:
        ctype, _ = discord.utils.find(lambda x: perm in x[1], perm_channel_types.items())
        return isinstance(channel, ctype)
    except TypeError:
        return True


# TODO this should probably be re-written to not be incredibly confusing to use, let alone maintain
@cog_i18n(_)
class PermBD(Toolset, name=_("Permission Breakdown")):
    """Breakdown the permissions a member is granted from their roles"""

    @commands.command(aliases=["permbd"])
    @commands.guild_only()
    async def permissionbreakdown(
        self,
        ctx: commands.Context,
        member: discord.Member = None,
        channel: Union[discord.TextChannel, discord.VoiceChannel, discord.CategoryChannel] = None,
        list_all: bool = False,
    ):
        """Breakdown the permissions a member is granted by their roles and overwrites"""
        member = member or ctx.author
        channel = channel or ctx.channel

        role_perms: Dict[str, List[discord.Role]] = {
            x: [
                r
                for r in reversed(member.roles)
                if getattr(r.permissions, str(x), False) is True
                or r.permissions.administrator is True
            ]
            for x, y in discord.Permissions()
            if list_all or is_applicable(x, channel)
        }

        pages = list(chunks(list(role_perms.items()), ceil(len(role_perms) / 5)))
        total_pages = len(pages)
        for page in pages.copy():
            index = pages.index(page)
            page = self.__converter(
                page=page,
                member=member,
                channel=channel,
                page_id=index + 1,
                total_pages=total_pages,
                colour=await ctx.embed_colour(),
            )
            pages[index] = page

        await MenuPages(ListPageSource(pages, per_page=1), clear_reactions_after=True).start(ctx)

    def __converter(
        self,
        channel: discord.TextChannel,
        member: discord.Member,
        page: dict,
        page_id: int,
        total_pages: int,
        colour: Union[discord.Colour, type(discord.Embed.Empty)] = discord.Embed.Empty,
    ):
        embed = discord.Embed(
            colour=colour,
            title=_("Permissions Breakdown").format(),
            description=_("Permissions for {member} in {channel}").format(
                channel=channel.mention, member=member.mention
            ),
        )

        for perm, roles in page:
            list_roles = self.__trim_to_three(roles) if roles else []
            value = [
                _(
                    "Granted by {n, plural, one {one role} other {# roles}} \N{EM DASH} {roles}"
                ).format(n=len(roles), roles=Humanize(list_roles))
                if roles
                else _("Not granted by any roles").format()
            ]

            overwrites = self.__build_overwrites(channel, member, perm)
            value.extend(
                [
                    _(
                        "{mode, select, granted {Granted} other {Denied}} by {n, plural,"
                        " one {one overwrite} other {# overwrites}} for \N{EM DASH} {overwrites}"
                    ).format(
                        mode=tid,
                        n=len(ow_roles),
                        overwrites=Humanize(self.__trim_to_three(ow_roles)),
                    )
                    for tid, ow_roles in overwrites.items()
                    if ow_roles
                ]
            )

            value.append(
                _(
                    "Final resolved value \N{EM DASH} **{value, select, granted {Granted}"
                    " other {Denied}}**"
                ).format(
                    value="granted"
                    if getattr(channel.permissions_for(member), perm, False)
                    else "denied"
                )
            )

            embed.add_field(name=format_permission(perm), value="\n".join(value), inline=False)

        return embed.set_footer(
            text=_("Page {current} out of {total}").format(current=page_id, total=total_pages)
        )

    @staticmethod
    def __build_overwrites(channel: discord.abc.GuildChannel, member: discord.Member, perm: str):
        overwrites: Dict[Union[discord.Member, discord.Role], Optional[bool]] = {
            x: getattr(y, perm, None)
            for x, y in getattr(channel.overwrites, "items", lambda: channel.overwrites)()
            if x == member or x in member.roles and getattr(y, perm, None) is not None
        }
        return {
            "granted": [k for k, v in overwrites.items() if v is True],
            "denied": [k for k, v in overwrites.items() if v is False],
        }

    @staticmethod
    def __trim_to_three(roles: List[discord.Role]) -> List[str]:
        new_list = [
            # Members and roles that aren't @everyone are mentioned as normal,
            # but @everyone is just listed with the name, to prevent it showing
            # as @@everyone.
            (x.mention if not isinstance(x, discord.Role) or not x.is_default() else x.name)
            for x in roles[:3]
        ]
        if len(roles) > 3:
            new_list.append(_("+{n} more").format(n=Humanize(len(roles) - 3)))
        return new_list
