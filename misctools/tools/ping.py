from babel.units import format_unit

from misctools import commands
from misctools.shared import _
from misctools.toolset import Toolset
from red_icu import Humanize, cog_i18n


@cog_i18n(_)
class PingTime(Toolset, name=_("Ping Time")):
    """Replaces `[p]ping` with a variation that displays latency to Discord"""

    # overwrite_existing is an attribute added by the MiscTools command extensions
    @commands.command(aliases=["pingtime"], overwrite_existing=True)
    async def ping(self, ctx: commands.Context):
        """Pong!"""
        latency = dict(self.bot.latencies)[ctx.guild.shard_id if ctx.guild else 0] * 1000
        await ctx.send(
            _(
                "\N{TABLE TENNIS PADDLE AND BALL} Pong! Current latency to Discord is {latency}"
            ).format(latency=Humanize(format_unit, latency, "millisecond"))
        )
