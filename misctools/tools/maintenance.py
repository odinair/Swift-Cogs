from __future__ import annotations

import asyncio
import os
import shlex
import sys
from argparse import Namespace
from collections import defaultdict
from typing import Dict, Optional, Sequence, Union

from redbot.core import checks
from redbot.core.commands import UserFeedbackCheckFailure
from redbot.core.utils.chat_formatting import info, pagify, warning
from redbot.vendored.discord.ext import menus

from misctools import commands
from misctools.shared import Arguments, _, log
from misctools.toolset import LoadMethod, Toolset, register_defaults
from misctools.utils import tick
from red_icu import cog_i18n


class MaintenanceEnabled(commands.CheckFailure):
    pass


class MaintenanceManager:
    __slots__ = ("_bot", "cogs")

    def __init__(self):
        self._bot = False
        self.cogs: Dict[str, Union[bool, str]] = defaultdict(lambda: False)

    @property
    def bot(self) -> Union[bool, str]:
        if "RED_MAINTENANCE" in os.environ:
            return self._bot if isinstance(self._bot, str) else True
        return self._bot

    @bot.setter
    def bot(self, value: Union[bool, str]):
        self._bot = value


manager = MaintenanceManager()


class UpdateRedMenu(menus.Menu):
    def __init__(self, *args, **kwargs):
        self.args: Namespace = kwargs.pop("namespace")
        super().__init__(*args, **kwargs)

    async def send_initial_message(self, ctx, channel):
        return await channel.send(
            warning(
                _(
                    "By continuing with this action, I will execute the following command on your"
                    " host machine:\n\n```sh\n{command}\n```\n\n"
                    "Updating while the bot is running may have a chance to break your Red install,"
                    " regardless of how unlikely it may be. You should consider playing it safe and"
                    " updating manually.\n\n"
                    "If you wish to continue, react to this message with \N{WHITE HEAVY CHECK MARK}"
                ).format(command=" ".join(["<python interpreter>", *self.cli_args[1:]]))
            )
        )

    @property
    def package(self) -> str:
        if self.args.dev:
            branch = (
                "V3/develop"
                if not any([self.args.version, self.args.branch])
                else (self.args.version or self.args.branch)
            )
            package = (
                f"https://github.com/Cog-Creators/Red-DiscordBot/tarball/{branch}"
                f"#egg=Red-DiscordBot"
            )
        else:
            package = "red-discordbot"

        if self.args.version and not self.args.dev:
            package += f"=={self.args.version}"

        optionals = [
            x for x in ("docs", "test", "postgres", "all") if getattr(self.args, x) is True
        ]
        if self.args.develop_extras:
            optionals += ["dev"]
        if optionals:
            package += "[{}]".format(", ".join(optionals))
        return package

    @property
    def cli_args(self) -> Sequence[str]:
        cli_args = [sys.executable, "-m", "pip", "install", "-U", self.package]
        if self.args.force:
            cli_args.insert(5, "--force-reinstall")
            cli_args.insert(5, "--no-cache-dir")
        return cli_args

    @menus.button("\N{WHITE HEAVY CHECK MARK}")
    async def update_red(self, __):
        await self.message.edit(content=info(_("Updating Red... (this may take a while)").format()))
        async with self.ctx.channel.typing():
            log.info("Updating Red...")
            p = await asyncio.create_subprocess_exec(*self.cli_args, stdin=None, loop=self.bot.loop)
            await p.wait()

        await self.message.delete()
        self.message = await self.ctx.channel.send(
            (
                _(
                    "Red has been successfully updated. Please restart the bot for the"
                    " updates to take effect."
                )
                if p.returncode == 0
                else _("Failed to update Red; check your logs for more information.")
            ).format()
        )
        self.stop()

    @menus.button("\N{CROSS MARK}")
    async def cancel(self, __):
        await self.message.edit(content=info(_("Cancelled.").format()))
        self.stop()


@cog_i18n(_)
class Maintenance(Toolset, name=_("Maintenance")):
    """Maintenance mode utility and Red updates"""

    @LoadMethod()
    async def bootstrap(self):
        register_defaults(maintenance__global=False, maintenance__cogs={})
        self.bot.add_check(self.global_check)

        data = await self.config.maintenance.all()
        # Convert older configurations
        if "enabled" in data:
            data["global"] = (
                data["message"] if data.get("message") and data["enabled"] else data["enabled"]
            )
            del data["enabled"]
            if "message" in data:
                del data["message"]
            await self.config.maintenance.set(data)

        manager.bot = data.get("global", False)
        manager.cogs.update(data.get("cogs", {}))
        log.debug("Maintenance toolset ready")

    def toolset_cleanup(self):
        self.bot.remove_check(self.global_check)
        manager.bot = False
        manager.cogs.clear()

    async def global_check(self, ctx: commands.Context) -> bool:
        if await self.bot.is_owner(ctx.author):
            return True

        message: Optional[str] = None
        if manager.bot:
            message = (
                manager.bot
                if isinstance(manager.bot, str)
                else _(
                    "\N{WARNING SIGN} I'm currently in maintenance mode; please try again later."
                ).format()
            )
        elif ctx.cog:
            name = type(ctx.cog).__name__.lower()
            if manager.cogs[name] is not False:
                message = (
                    manager.cogs[name]
                    if isinstance(manager.cogs[name], str)
                    else _(
                        "\N{WARNING SIGN} Maintenance mode is currently enabled for the **{cog}**"
                        " cog; please try again later."
                    ).format(cog=type(ctx.cog).__name__)
                )

        if message is not None:
            raise UserFeedbackCheckFailure(message)
        return True

    @commands.group()
    @checks.is_owner()
    async def maintenance(self, ctx: commands.Context):
        """Manage bot maintenance modes"""

    @maintenance.command(name="enable")
    async def maintenance_enable(self, ctx: commands.Context, *, message: str = None):
        """Enable bot-wide maintenance mode"""
        manager.bot = message or True
        await self.config.maintenance.set_raw("global", value=message or True)
        await ctx.send(tick(_("Bot is now in maintenance mode.").format()))

    @maintenance.command(name="disable")
    @commands.check(lambda ctx: manager.bot is not False)
    async def maintenance_disable(self, ctx: commands.Context):
        """Disable bot-wide maintenance mode"""
        if os.environ.get("RED_MAINTENANCE"):
            await ctx.send(
                warning(
                    _(
                        "Maintenance mode is enabled with the `RED_MAINTENANCE` environment"
                        " variable; unset it and restart the bot to disable maintenance mode."
                    ).format()
                )
            )
            return

        manager.bot = False
        await self.config.maintenance.set_raw("global", value=False)
        await ctx.send(tick(_("Disabled bot-wide maintenance mode.").format()))

    @maintenance.command(name="cog", usage="(enable|disable) <cog> [message]")
    async def maintenance_cog(
        self, ctx: commands.Context, status: str, cog: str, *, message: str = None
    ):
        """Enable or disable maintenance mode for a specific cog

        Disabling maintenance mode for a single cog doesn't exempt it from a bot-wide maintenance.
        """
        status = status in ("enabled", "enable", "1", "yes", "true", "on")
        cog = next(iter(x.lower() for x in self.bot.cogs.keys() if x.lower() == cog.lower()), None)
        if cog is None:
            await ctx.send(_("No such cog with that name exists.").format())
            return

        manager.cogs[cog] = message if message and status else status
        if not status:
            await self.config.maintenance.cogs.set_raw(cog, value=False)
            await ctx.send(_("`{cog}` is no longer in maintenance mode.").format(cog=cog))
        else:
            await self.config.maintenance.cogs.set_raw(cog, value=message or True)
            await ctx.send(_("`{cog}` is now in maintenance mode.").format(cog=cog))

    @maintenance.command(name="status", aliases=["info"])
    async def maintenance_info(self, ctx: commands.Context):
        """Check maintenance mode for [botname]"""
        cogs = {
            next(x for x in self.bot.cogs if x.lower() == k): v
            if isinstance(v, str)
            else _("No message set").format()
            for k, v in manager.cogs.items()
            if v and any(x.lower() == k for x in self.bot.cogs)
        }

        message = []
        if manager.bot:
            message.append(_("Bot-wide maintenance mode is currently on").format())
        else:
            message.append(_("Bot-wide maintenance mode is currently off").format())
        if manager.bot and isinstance(manager.bot, str):
            message.append(
                _("Message sent on command attempt: {message}").format(message=manager.bot)
            )
        message.append("")

        if cogs:
            message.append(
                _(
                    "{count, plural, one {{count} cog is} other {{count} cogs are}} currently in"
                    " maintenance mode:"
                ).format(count=len(cogs))
            )
            for cog, msg in cogs.items():
                message.append(f"    \N{BULLET} **`{cog}`** \N{EM DASH} {msg}")

        for page in pagify(info("\n".join(message))):
            await ctx.send(page)

    @commands.command()
    @checks.is_owner()
    async def updatered(self, ctx: commands.Context, *, args: str = ""):
        """Update Red to the latest version

        This command uses CLI-like flags to determine how to update Red.

        **Available flags:**
        **-p | --postgres** \N{EM DASH} Installs PostgreSQL config storage dependencies
        **-A | --all** \N{EM DASH} Update Red with all extras
        **-V | --version <version>** \N{EM DASH} Specify a Red version to install

        **Advanced options:**
        **-d | --dev** \N{EM DASH} Pulls the latest changes from GitHub
        **-D | --develop-extras** \N{EM DASH} Update Red with all development extras
        **--docs** \N{EM DASH} Sphinx documentation library
        **--test** \N{EM DASH} Testing related dependencies
        **--branch <branch>** \N{EM DASH} Specify a different Git branch to use; implies `--dev`
        **--force** \N{EM DASH} Bypass the cache and force reinstall
        """
        parser = Arguments(add_help=False)
        parser.add_argument("-d", "--dev", "--git", action="store_true")
        parser.add_argument("-A", "--all", action="store_true")
        parser.add_argument("-p", "--postgres", "--psql", action="store_true")
        parser.add_argument("-D", "--develop-extras", action="store_true")
        parser.add_argument("--docs", action="store_true")
        parser.add_argument("--test", action="store_true")
        parser.add_argument("-V", "--version", type=str, default=None)
        parser.add_argument("--branch", type=str, default=None)
        parser.add_argument("--force", action="store_true")
        args = parser.parse_args(shlex.split(args))

        await UpdateRedMenu(namespace=args).start(ctx)
