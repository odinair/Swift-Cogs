from .dev import Dev
from .maintenance import Maintenance
from .permbd import PermBD
from .ping import PingTime
from .uinfo import UInfo
from .vcrole import VCRole

"""
################# READ ME #################

MiscTools is largely designed to be a compilation of commands that can be quickly
patched together (usually within 10-20 minutes of the original concept being made).

As such, most everything that normally just works in cogs doesn't exactly "just work"
in toolsets. This includes listeners, cog-level error handlers, and (global) checks,
which means you'll have to add/remove them on setup/cleanup on your own.

Additionally, there's a few implementation quirks you'll want to know:

- If you can avoid it, use the `LoadMethod` decorator instead of creating a task on toolset init.
  The core loading logic will handle the rest for you.
- Use `toolset_unload` in place of the traditional `cog_unload` method. Similarly to cog_unload,
  this method may not be async.
- It's strongly recommended to use the `name` class kwarg to specify a LazyStr object
  which refers to your toolset's name. The class name will still be used to refer to the
  toolset in commands, however.

################# READ ME #################
"""

# noinspection PyUnresolvedReferences
from misctools.toolset import toolsets
