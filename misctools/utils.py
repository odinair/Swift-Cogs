from typing import Any, Sequence


def flatten_list(lst: Sequence[Any]):
    for item in lst:
        if isinstance(item, (list, tuple, set)):
            yield from flatten_list(item)
        else:
            yield item


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"
