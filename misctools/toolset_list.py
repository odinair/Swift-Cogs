from typing import List, Type

import discord
from redbot.vendored.discord.ext import menus

from misctools.shared import _
from misctools.toolset import Toolset
from red_icu import Humanize


class ToolsetListPageSource(menus.ListPageSource):
    def __init__(self, entries, loaded: List[Toolset], *, per_page):
        super().__init__(entries, per_page=per_page)
        self._loaded = loaded

    def _is_loaded(self, toolset: Type[Toolset]) -> bool:
        return any(type(t) is toolset for t in self._loaded)

    async def format_page(self, menu: menus.MenuPages, page: Type[Toolset]):
        loaded = self._is_loaded(page)
        desc = [
            str(page.doc()) or _("*No description provided*"),
            "",
            _("**Currently loaded** \N{EM DASH} {loaded, select, y {Yes} other {No}}").format(
                loaded="y" if loaded else "n"
            ),
            _("**Total commands** \N{EM DASH} {count}").format(
                count=Humanize(len([x for x in page.__commands__.values() if not x.parent]))
            ),
        ]
        if page.__hidden__:
            desc.insert(2, _("**Hidden** \N{EM DASH} Yes").format())
        embed = discord.Embed(
            colour=discord.Colour.green() if loaded else discord.Colour.greyple(),
            title=str(page.__toolset_name__),
            description="\n".join(desc),
        )

        commands = []
        for cmd in page.__commands__.values():
            if not cmd.enabled:
                continue
            doc = cmd.short_doc
            doc = doc.replace("[botname]", menu.ctx.bot.user.name)
            doc = doc.replace("[p]", menu.ctx.clean_prefix)
            commands.append(f"**`{cmd.qualified_name}`** \N{EM DASH} {doc}")

        embed.add_field(
            name=_("Commands").format(),
            value="\n".join(commands),
            inline=False,
        )

        embed.add_field(
            name="\N{ZERO WIDTH JOINER}",
            value=(
                _("**To load this toolset** \N{EM DASH}\n```\n{command}\n```")
                if not loaded
                else _("**To unload this toolset** \N{EM DASH}\n```\n{command}\n```")
            ).format(
                command=f"{menu.ctx.clean_prefix}misctools {'load' if not loaded else 'unload'}"
                f" {page.__name__.lower()}"
            ),
            inline=False,
        )

        embed.set_footer(
            text=_("Toolset {current} out of {total}").format(
                current=menu.current_page + 1, total=self.get_max_pages()
            )
        )
        return embed
