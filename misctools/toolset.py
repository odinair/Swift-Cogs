from __future__ import annotations

from inspect import cleandoc
from typing import Any, Callable, ClassVar, Dict, List, Optional, Union

from redbot.core import Config, commands
from redbot.core.bot import Red

from misctools.commands import _Command, _CommandMixin, _Group
from misctools.shared import config
from red_icu import Translator
from red_icu.util import LazyStr

defaults: Dict[str, Dict[str, Any]] = {Config.GLOBAL: {"toolsets": []}}
toolsets = []


def rebuild_defaults() -> None:
    for scope, values in defaults.items():
        config.register_custom(scope, **values)


def register_defaults(config_scope: str = Config.GLOBAL, **kwargs):
    if config_scope not in defaults:
        defaults[config_scope] = {}
    defaults[config_scope].update(kwargs)
    rebuild_defaults()


class LoadMethod:
    """Mark a method as being required to be called on toolset load

    Decorated methods may be sync or async. Methods marked with this decorator are loaded
    before your toolset's commands have been added to the bot.

    .. important::
        If your load method requires the use of ``Bot.wait_until_ready`` or any similar
        methods that would cause issues when loading on bot startup, you should
        ensure that you specify the ``blocking`` keyword argument.

    .. warning::
        Methods decorated with this class that are marked as not blocking
        will not prevent your toolset from loading if they raise an error.
    """

    def __init__(self, *, blocking: bool = True):
        self.blocking = blocking

    def __set_name__(self, owner, name):
        if owner.__load_methods__ is None:
            owner.__load_methods__ = []
        owner.__load_methods__.append(self)

    def __call__(self, func: Callable[[], Any]) -> LoadMethod:
        if hasattr(self, "func"):
            raise RuntimeError("Load method function is already set")
        self.func = func
        self.__wrapped__ = func
        return self


class Toolset:
    __hidden__: ClassVar[bool] = False
    __load_methods__: ClassVar[Optional[List[LoadMethod]]] = None
    __translator__: ClassVar[Optional[Translator]] = None
    __replaces__: ClassVar[Optional[List[str]]] = None
    __toolset_name__: ClassVar[Union[str, LazyStr]] = None
    __commands__: ClassVar[Dict[str, Union[_Command, _Group]]]

    def __init__(self, bot: Red):
        self.bot = bot
        self.config = config

    def __init_subclass__(cls, **kwargs):
        if any(
            [
                x
                for x in cls.__dict__.values()
                if isinstance(x, commands.Command) and not isinstance(x, _CommandMixin)
            ]
        ):
            raise RuntimeError(
                "One or more of this toolset's commands do not use the MiscTools command classes"
            )

        cls.__toolset_name__ = kwargs.get("name", cls.__toolset_name__ or cls.__name__)
        cls.__hidden__ = kwargs.get("hidden", cls.__hidden__)
        cls.__replaces__ = kwargs.get("replaces", cls.__replaces__)
        cls.__commands__ = {
            x.qualified_name: x for x in cls.__dict__.values() if isinstance(x, commands.Command)
        }
        toolsets.append(cls)

    @classmethod
    def doc(cls) -> Optional[str]:
        # red_icu makes this a callable that directly returns a str instead of LazyStr
        if cls.__translator__:
            class_kwargs = getattr(cls, "__icu_format_args__", {})
            return cls.__translator__(cleandoc(cls.__doc__), **class_kwargs)
        return cls.__doc__

    def toolset_cleanup(self):
        """Hook called after your toolset's commands are unloaded

        This method must not be async.
        """
        pass
