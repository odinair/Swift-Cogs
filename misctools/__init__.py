import json
from pathlib import Path

from redbot.core.bot import Red

with open(str(Path(__file__).parent / "info.json")) as f:
    __red_end_user_data_statement__ = json.load(f)["end_user_data_statement"]


async def setup(bot: Red):
    from misctools.misctools import MiscTools, log

    cog = MiscTools(bot)
    bot.add_cog(cog)
    try:
        await cog.loader.bootstrap()
        log.debug("Cog load bootstrap complete")
    except Exception:
        try:
            cog.loader.cog_unload()
        finally:
            bot.remove_cog(cog.__class__.__name__)
        raise
