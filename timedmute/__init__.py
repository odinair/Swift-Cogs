import json
from pathlib import Path

from redbot.core.bot import Red
from redbot.core.errors import CogLoadError

with open(str(Path(__file__).parent / "info.json")) as f:
    __red_end_user_data_statement__ = json.load(f)["end_user_data_statement"]


def setup(bot: Red):
    from timedmute.timedmute import TimedMute, _

    if "TimedRole" not in bot.cogs:
        raise CogLoadError(_("This cog requires `timedrole` to be loaded to function.").format())

    bot.add_cog(TimedMute(bot))
