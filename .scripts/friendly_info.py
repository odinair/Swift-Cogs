#!/usr/bin/env python
import argparse
import json
import logging
import os
import re
import sys
import unicodedata
import warnings
from copy import deepcopy
from glob import glob
from inspect import cleandoc
from pathlib import Path
from typing import Optional, Sequence, Union

import toml

# region Utils
unicode_regx = re.compile(r"(?:\\N|\x85){(?P<NAME>[A-Z -]+)}")
linebr = re.compile(r" ?\\\n[ ]*")


def escape_linebrs(text: str) -> str:
    return linebr.sub(" ", text).strip()


def parse_unicode(line: str, strict: bool = False) -> str:
    if not isinstance(line, str):
        return line

    chars = {(x.group(0), x.group("NAME")) for x in unicode_regx.finditer(line)}
    for orig, char in chars:
        try:
            replacement = unicodedata.lookup(char)
        except KeyError:
            if strict:
                raise
            warnings.warn(f"Failed to find any unicode character by the name {char!r}", UserWarning)
        else:
            line = line.replace(orig, replacement)
    return line


def deep_merge(source: dict, destination: dict, *, merge_lists: Union[bool, Sequence[str]] = False):
    # original: https://stackoverflow.com/a/20666342
    for key, value in source.items():
        if isinstance(value, dict):
            node = destination.setdefault(key, {})
            deep_merge(value, node, merge_lists=merge_lists)
        elif (
            merge_lists
            and isinstance(value, list)
            and key in destination
            and isinstance(destination[key], list)
            and (
                not isinstance(  # pylint:disable=isinstance-second-argument-not-valid-type
                    merge_lists, Sequence
                )
                or key in merge_lists
            )
        ):
            for item in value:
                if item in destination[key]:
                    continue
                destination[key].append(item)
        else:
            destination[key] = value

    return destination


def clean(text):
    if not isinstance(text, str):
        return text
    return cleandoc(escape_linebrs(parse_unicode(text)))


# endregion
__version__ = "3.1.0"

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("main")
parser = argparse.ArgumentParser(prog="friendly_info.py")
parser.add_argument(
    "-V", "--version", help="print the script version and exit", action="store_true"
)
parser.add_argument(
    "-d",
    "--meta-dir",
    help="directory where your info.json meta files reside",
    default=".meta",
)
parser.add_argument("--verbose", action="store_true", help="enable debug logging")
parser.add_argument(
    "--dry-run",
    action="store_true",
    help="simulate generating info.json files, but don't write anything to disk",
)


def _load(path: Path) -> dict:
    """Load a given TOML file and return it as a dict

    If given a path that doesn't exist or is a directory, this function will
    log an error and return an empty dict.
    """
    if not path.exists():
        log.error("Cannot read %r - file does not exist", str(path))
        return {}
    if path.is_dir():
        log.error("Cannot read %r - path is a directory", str(path))
        return {}
    with open(path.resolve()) as f:
        return toml.load(f)


def _find_include(parent: Path, name: str) -> Optional[Path]:
    """Find an include file with the given name

    This attempts to look for the file name exactly, and resorts
    to trying `.{name}.toml` if that doesn't exist.
    """
    if (path := parent / name).exists():
        return path
    if (path := parent / f".{name}.toml").exists():
        return path
    return None


def _gen_info(file: Path, global_mixins: dict = None) -> Optional[dict]:
    """Generate an info.json dict from the given meta file and global mixins"""
    data = _load(file)
    name = file.name.replace(".toml", "")
    log.debug("Generating info.json for %r", name)

    if include := data.pop("__include__", None):
        for to_include in include:
            include_file = _find_include(file.parent, to_include)
            if not include_file:
                log.warning(
                    "Cannot include %r - an include file with that name doesn't exist", to_include
                )
                continue
            log.info("Including file %r in %r", str(include_file), name)
            data = deep_merge(data, _load(include_file), merge_lists=True)

    # This is after includes to allow for includes to exclude global properties,
    # for example with `requirements`.
    if global_mixins and not data.pop("__no_globals__", False):
        global_mixins = deepcopy(global_mixins)
        if excludes := data.pop("__exclude__", None):
            for exclude in excludes:
                try:
                    log.debug("Deleting exclude %r", exclude)
                    del global_mixins[exclude]
                except KeyError:
                    log.warning(
                        "Invalid exclude in %r - %r does not exist in globals", name, exclude
                    )
        data = deep_merge(data, global_mixins, merge_lists=["requirements"])
    else:
        log.debug("No globals exist or __no_globals__ is set")
        data.pop("__exclude__", None)

    for to_clean in ("short", "description", "install_msg", "end_user_data_statement"):
        if to_clean not in data:
            continue
        log.debug("Cleaning %r", to_clean)
        data[to_clean] = clean(data[to_clean])
    if "short" not in data and "description" in data:
        log.debug("Adding short description for %r from long description", name)
        data["short"] = data["description"].split("\n")[0]

    return data


def _write_info(
    meta_file: Path, global_mixins: dict = None, *, output_dir: Path = None, dry_run: bool = False
) -> None:
    """Load a meta file and write the resulting info.json file"""
    data = _gen_info(meta_file, global_mixins)
    if output_dir is None:
        if output := data.pop("__output__", None):
            output_dir = (Path(os.getcwd()) / output).resolve()
        else:
            output_dir = Path(os.getcwd()) / meta_file.name.replace(".toml", "")

    if not output_dir.exists():
        log.warning("Cannot write info.json into %r - directory does not exist", str(output_dir))
        return
    if not output_dir.is_dir():
        log.warning("Cannot write info.json into %r - not a directory", str(output_dir))
        return

    indent = data.pop("__indent__", 0)
    write_to = str(output_dir / "info.json")
    if dry_run is False:
        with open(write_to, mode="w") as f:
            log.info("Writing to %r", write_to)
            json.dump(data, f, indent=indent)
    else:
        log.info("Would have written %r to %r", data, write_to)


def main():
    meta_dir = Path(args.meta_dir).absolute()
    log.debug("Looking in directory %r", meta_dir)
    global_vals = None
    if (global_file := (meta_dir / ".globals.toml")).exists():
        global_vals = _load(global_file)
        log.debug("Loaded globals: %r", global_vals)

    for meta in glob(os.sep.join([str(meta_dir), "**", "*.toml"]), recursive=True):
        # Meta files starting with a . are reserved for includes
        if meta.startswith("."):
            continue
        meta = Path(meta)
        _write_info(meta, global_vals, dry_run=args.dry_run)


if __name__ == "__main__":
    args = parser.parse_args()
    if args.verbose:
        log.setLevel(logging.DEBUG)
    if args.version:
        print(f"friendly_info.py {__version__}")
        sys.exit()
    main()
