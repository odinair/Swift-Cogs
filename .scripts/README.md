# Utility Scripts

A small collection of hacked together utility scripts to help with cog development.

## gen_locales.py

Generates cog locale message stubs with `redgettext`.

```sh
# Newer/older versions may work, but will only be supported on an as-is basis.
pip install redgettext==3.3
python gen_locales.py [cog...] [--verbose] [--docstrings]
```

## friendly_info.py

This script reads `.toml` files from a `.meta` directory to generate `info.json` files for use
in Red cogs.

This is designed to help with large repositories that have many cogs to maintain,
especially where manually updating the `info.json` files for manually would be excessively
tedious to update the minimum Red version needed, or to add/update a core library used by
all cogs in a repository.

This script additionally cleans up the `short`, `description`, `install_msg`, and `end_user_data_statement`
properties for you. This includes replacing raw `\n` escape sequences with line breaks,
and parsing named unicode character escape sequences (such as `\N{PURPLE HEART}`).

```sh
# be sure to read further below about the .meta directory before attempting to
# use this script!

# by default without any arguments, this generates all info.json files in .meta;
# this can be changed by specifying cog names when running this script
python friendly_info.py [cog...] [-V|--version] [-d|--meta-dir META_DIR] [--verbose] [--dry-run]
```

### The Meta Directory

The meta directory is a collection of `.toml` files which denote how info.json files are generated.
Files starting with a `.` character are ignored, and as such may be used for includes (more on this below).

The `.globals.toml` file has special importance - all\* values in it are automatically
copied into all generated info.json files.  
\* specific keys can be excluded as needed - more on this below.

An example directory layout is as follows:

<!-- markdownlint-disable MD040 -->
```
/.meta/
  /.globals.toml   # variables included into all generated info.json files (with exceptions; see __exclude__)
  /.include.toml   # .toml files starting with a . will not output an info.json file
  /repo.toml       -> /info.json  (requires use of __output__)
  /example.toml    -> /example/info.json
  /folder/         # .toml files may be nested as deeply as desired
    /file.toml     -> /file/info.json
```
<!-- markdownlint-enable MD040 -->

The following keys have special importance when included in meta files, and are not included
in the generated info.json files but instead change how they're generated:

- `__include__` - Includes data from the given files into the file currently being generated. This has no effect when used in includes.
    *Example: `include = [".include.toml"]`*
- `__exclude__` - Exclude specific globals keys (and *only* keys from `.globals.toml`!)
    *Example: `exclude = ["requirements"]`*
- `__indent__` - How many indents to use when generating the output info.json files; defaults to 0.
    *Example: `__indent__ = 2`*
- `__output__` - The directory to output this file into, relative to the current working directory.
    *Example: `__output__ = "mycog"` or `__output__ = "."`*
- `__no_globals__` - If this is `true`, `.globals.toml` will not be included in the output file.
    *Example: `__no_globals__ = true`*

When using `__include__`, everything is merged, including lists. This however is not the case
when it comes to globals - only the `requirements` list is merged.
