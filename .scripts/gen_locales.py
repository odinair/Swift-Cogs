#!/usr/bin/env python3
"""a wrapper script around redgettext for bulk updating cog locale stubs

written by odinair#0001

basic usage:
    $ python3 gen_locales.py [cog [cog ...]] [--docstrings] [--verbose]

if no cogs are specified, the current directory is walked and directories that contain
a folder matching the specified output directory (or `locales` if no directory is specified)
are treated as the cogs to update locale stubs for
"""

import argparse
import os
import subprocess
import sys
from glob import glob
from pathlib import Path

root = Path(os.getcwd())
parser = argparse.ArgumentParser()
parser.add_argument(
    "cogs",
    nargs="*",
    help="the cogs to build locales for. if no cogs are given, the current working "
    "directory is scanned for directories containing a locales directory",
)
parser.add_argument("--verbose", help="enables verbose logging", action="store_true")
parser.add_argument(
    "--docstrings", action="store_true", help="extract all docstrings, not just commands"
)


def scan_dir() -> set:
    return (
        {
            x[0].split("/")[-1]
            for x in os.walk(str(root))
            if len(x[0].replace(str(root), "").split("/")) == 2
            and (Path(x[0].split("/")[-1]) / "locales").exists()
        }
        if not args.cogs
        else {*args.cogs}
    )


if __name__ == "__main__":
    args = parser.parse_args()
    cogs = {x for x in scan_dir() if (Path(x) / "locales").exists()}
    generated = 0

    for cog in cogs:
        print("Generating locales for cog {}".format(cog))
        path = str(root / cog)
        if args.verbose:
            print("[verb] cd:   {}".format(path))
        os.chdir(path)

        # --relative-to-cwd prevents redgettext from creating multiple locales directories
        # when we want it to only use the one locales directory in the cog root
        cmd = [sys.executable, "-m", "redgettext", "--relative-to-cwd"]
        cmd += ["--command-docstrings"] if not args.docstrings else ["--docstrings"]
        # this glob as necessary as cogs that use translation in files that aren't
        # in the root cog path aren't correctly picked up otherwise
        cmd += ["-n", *glob("**/*.py", recursive=True)]
        if args.verbose:
            print("[verb] exec: {}".format(" ".join(cmd)))
        subprocess.call(cmd)
        generated += 1
    print(f"Done; generated locales for {generated} cog{'s' if generated != 1 else ''}.")
