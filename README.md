<div align="center">
    <h1>Swift Cogs</h1>
    A curated collection of moderation and utility cogs for [Red](https://github.com/Cog-Creators/Red-DiscordBot) (with a few community-oriented cogs).
</div>

## Install

All the cogs in this repository depend on an [externally hosted dependency](https://gitlab.com/odinair/red_icu)
which is automatically downloaded and installed when you install one of these cogs. This may as a result slow down
updating on bots with many of these cogs installed, due to how Downloader handles requirements.

**Install instructions have been removed, as this repository is now considered abandoned,
and won't receive any updates going forward.**

*So long, and thanks for all the fish. :purple_heart:*

## Cogs

| Cog            | Description                                                                   |
| -------------- | ----------------------------------------------------------------------------- |
| `messagequote` | Quote a message by its ID or message link                                     |
| `misctools`    | Your trusty Swiss Army Knife of small but useful utilities                    |
| `overseer`     | Log everything that happens in your server with fine-grained settings         |
| `polls`        | Fancy persistent reaction polls                                               |
| `remindme`     | Procrastinate and leave that boring task for future you to take care of       |
| `rndactivity`  | Randomly select a playing status from a set list on an interval               |
| `starboard`    | Star quality messages so they'll never be forgotten to the depths of #general |
| `timedrole`    | Give users roles for a limited amount of time                                 |
