from asyncio import sleep
from datetime import datetime, timedelta
from typing import Iterable, List, Literal, MutableMapping, Sequence, Set, Tuple
from weakref import WeakKeyDictionary

import discord
from redbot.core import checks, commands
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import pagify, warning
from redbot.vendored.discord.ext import menus

from red_icu import Humanize, Translator, cog_i18n
from timedrole._shared import config
from timedrole.api import TempRole

_ = Translator("TimedRole", __file__)
TimeConverter = commands.get_timedelta_converter(minimum=timedelta(minutes=2), default_unit="hours")


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


def chunks(seq: Sequence, chunk_every: int) -> Iterable[Sequence]:
    for i in range(0, len(seq), chunk_every):
        yield seq[i : i + chunk_every]


class ListPageSource(menus.ListPageSource):
    def format_page(self, menu, page):
        return page


@cog_i18n(_)
class TimedRole(commands.Cog):
    """Add roles to members for a set amount of time"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        TempRole.bot = self.bot
        self._role_task = self.bot.loop.create_task(self._handle_roles())
        self._removing: MutableMapping[discord.Member, Set[discord.Role]] = WeakKeyDictionary()

    async def red_delete_data_for_user(
        self,
        *,
        requester: Literal["discord_deleted_user", "owner", "user", "user_strict"],
        user_id: int,
    ):
        # We aren't processing deletion requests from users due to the nature of
        # this cog being potentially used for moderation purposes.
        if requester not in ("discord_deleted_user", "owner"):
            return

        for gid, members in (await config.all_members()).items():
            if user_id in members:
                await config.member_from_ids(gid, user_id).clear()

    # noinspection PyPep8Naming
    @property
    def TempRole(self):
        return TempRole

    def cog_unload(self):
        self._role_task.cancel()

    async def _handle_roles(self):
        await self.bot.wait_until_ready()
        while True:
            self._removing = {}
            async for role in TempRole.all_roles():
                if role.expired:
                    if role.member not in self._removing:
                        self._removing[role.member] = set()
                    self._removing[role.member].add(role.role)
                    await role.remove_role()
                elif role.role not in role.member.roles:
                    await role.apply_role(reason=_("Re-applying missing timed role").format())
            await sleep(60)

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        if (
            not member.guild.me.guild_permissions.manage_roles
            or await self.bot.cog_disabled_in_guild(self, member.guild)
        ):
            return

        to_reapply = [x.role async for x in TempRole.all_roles(member) if not x.expired]
        if to_reapply:
            await member.add_roles(
                *to_reapply,
                reason=_(
                    "Re-applying {count, plural, one {timed role} other {timed roles}}"
                    " after member rejoin"
                ).format(count=len(to_reapply)),
            )

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        if not after.guild:
            return
        removed = [
            x
            for x in before.roles
            if x not in after.roles
            # set.discard() doesn't return anything, meaning this will be skipped
            and (x not in (r := self._removing.get(before, set())) or r.discard(x))
        ]
        for role in removed:
            trole = await TempRole.get(after, role)
            if not trole:
                continue
            await trole.delete()

    @commands.group(aliases=["temprole"])
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    @checks.bot_has_permissions(manage_roles=True)
    async def timedrole(self, ctx: commands.Context):
        """Manage active timed roles and give members new ones"""

    @timedrole.command(name="list")
    async def timedrole_list(self, ctx: commands.Context):
        """List all active timed roles"""
        pages = list(chunks([x async for x in TempRole.all_roles(ctx.guild)], 5))
        if not pages:
            await ctx.send(warning(_("This server has no active timed roles.").format()))
            return

        total_pages = len(pages)
        colour = await ctx.embed_colour()
        for page in pages.copy():
            index = pages.index(page)
            embed = (
                discord.Embed(colour=colour)
                .set_footer(
                    text=_("Page {current} out of {total}").format(
                        current=index + 1, total=total_pages
                    )
                )
                .set_author(name=_("Timed Roles"), icon_url=ctx.guild.icon_url)
            )

            for role in page:
                rid = ((index * 5) + page.index(role)) + 1

                value = [
                    _(
                        "{member} was given the role {role} by {given_by} {at_datetime},"
                        " and will expire {expiry_delta}."
                    ).format(
                        member=role.member.mention,
                        role=role.mention,
                        given_by=getattr(role.added_by, "mention", _("*unknown member*")),
                        at_datetime=Humanize(role.added_at - datetime.utcnow(), add_direction=True),
                        expiry_delta=Humanize(
                            role.expires_at - datetime.utcnow(), add_direction=True
                        ),
                    )
                ]
                if role.reason:
                    value.extend(
                        [
                            "",
                            _("Reason provided for this timed role: {reason}").format(
                                reason=role.reason
                            ),
                        ]
                    )

                embed.add_field(name=_("Role #{id}").format(id=rid), value="\n".join(value))
            pages[index] = embed

        await menus.MenuPages(ListPageSource(pages, per_page=1)).start(ctx)

    @staticmethod
    def _filter_invalid(
        ctx: commands.Context, member: discord.Member, roles: Sequence[discord.Role]
    ) -> Tuple[List[discord.Role], List[str]]:
        # use a set to de-dupe before we start checking
        roles = set(roles)

        above_my_top = []
        above_mod_top = []
        already_has = []

        # no pycharm, `ctx.me` does have a `top_role` attribute.
        # noinspection PyTypeChecker
        me: discord.Member = ctx.me

        for role in roles.copy():
            if role in member.roles:
                already_has.append(role)
            elif role >= ctx.author.top_role:
                above_mod_top.append(role)
            elif role >= me.top_role:
                above_my_top.append(role)
            else:
                continue
            roles.remove(role)

        def fmt(x: List[discord.Role], to_format):
            for r in x:
                yield to_format.format(name=r.mention)

        return (
            [*roles],
            [
                *fmt(
                    above_my_top,
                    _("Failed to add {role} \N{EM DASH} role is above my highest role"),
                ),
                *fmt(
                    above_mod_top,
                    _(
                        "Failed to add {role} \N{EM DASH} role is above command invoker's"
                        " highest role"
                    ),
                ),
                *fmt(
                    already_has,
                    _("Failed to add {role} \N{EM DASH} they already have that role"),
                ),
            ],
        )

    @timedrole.command(name="add", usage="<member> <duration> <role>...")
    async def timedrole_add(
        self,
        ctx: commands.Context,
        member: discord.Member,
        duration: TimeConverter,
        *roles: discord.Role,
    ):
        """Add one or more roles to a user for a set amount of time

        You may only give up to 10 roles per command invocation. This does not limit the total
        amount of timed roles a member can have.

        The minimum duration a timed role can last for is two minutes.
        """
        roles, skipped = self._filter_invalid(ctx, member, roles)
        if not roles and not skipped:
            await ctx.send_help()
            return

        if skipped:
            for pg in pagify("\n".join(skipped)):
                await ctx.send(pg, allowed_mentions=discord.AllowedMentions(roles=False))

        if len(roles) > 10:
            await ctx.send(
                _(
                    "This command only allows adding 10 roles at once, but you specified"
                    " {excess, plural, one {1 more role} other {{excess} more roles}} than"
                    " allowed."
                ).format(excess=len(roles) - 10)
            )
            return

        if not roles:
            return

        for role in roles:
            # noinspection PyTypeChecker
            role = await TempRole.create(member, role=role, duration=duration, added_by=ctx.author)
            await role.apply_role()

        await ctx.send(
            tick(
                _(
                    "Added {count, plural, one {{count} role} other {{count} roles}} to member"
                    " {member} for {duration}: {role_list}"
                ).format(
                    role_list=Humanize([*map(lambda x: x.mention, roles)]),
                    count=len(roles),
                    member=member.mention,
                    duration=Humanize(duration),
                )
            ),
            allowed_mentions=discord.AllowedMentions(users=False, roles=False),
        )

    @timedrole.command(name="remove")
    async def timedrole_expire(
        self, ctx: commands.Context, member: discord.Member, *roles: discord.Role
    ):
        """Remove one or all active timed roles from a member

        This can also be done by simply removing the role from the member like any other role.
        """
        mroles = [x async for x in TempRole.all_roles(ctx.guild, member)]
        removed = []
        for r in mroles:
            if not roles or r.role in roles:
                await r.remove_role(
                    reason=_("Role manually removed by {member}").format(member=str(ctx.author))
                )
                removed.append(r)

        if not removed:
            if not roles:
                await ctx.send(
                    _("{member} has no timed roles to remove.").format(member=member.mention),
                    allowed_mentions=discord.AllowedMentions(users=False),
                )
            else:
                await ctx.send(
                    _(
                        "{specified, plural, one {The specified role couldn't}"
                        " other {None of the specified roles could}} be removed from {member}."
                    ).format(member=member.mention, specified=len(roles)),
                    allowed_mentions=discord.AllowedMentions(users=False),
                )
            return
        await ctx.send(
            _(
                "Removed {count, plural, one {1 timed role} other {{count} timed roles}}"
                " from member {member}."
            ).format(member=member.mention, count=len(removed)),
            allowed_mentions=discord.AllowedMentions(users=False),
        )
