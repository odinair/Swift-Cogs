import logging

from red_icu import Translator

icu = Translator("RNDActivity", __file__)
log = logging.getLogger("red.rndactivity")


@icu.transformer
def _placeholder_transformer(__, kwargs: dict):
    from rndactivity.placeholders import Placeholders

    for k, v in kwargs.copy().items():
        if v is Placeholders:
            kwargs[k] = str(v())
