import re
from asyncio import sleep
from datetime import timedelta
from random import choice
from typing import List, Union, Optional, Tuple

import discord
from redbot.core import Config, checks, commands
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import escape, pagify, warning
from redbot.vendored.discord.ext import menus

from rndactivity.placeholders import Placeholders
from rndactivity.shared import icu as _, log
from red_icu import Humanize, cog_i18n, command_format_args

TWITCH_REGEX = re.compile(r"<?(?:https?://)?twitch\.tv/[^ ]+>?", re.IGNORECASE)
AVAILABLE_ACTIVITY_TYPES = {
    "playing": discord.ActivityType.playing,
    "listening": discord.ActivityType.listening,
    "watching": discord.ActivityType.watching,
    "competing": discord.ActivityType.competing,
    "streaming": discord.ActivityType.streaming,
}
GAME_TYPES = {
    discord.ActivityType.playing: _("Playing"),
    discord.ActivityType.listening: _("Listening to"),
    discord.ActivityType.watching: _("Watching"),
    discord.ActivityType.competing: _("Competing in"),
    discord.ActivityType.streaming: _("Streaming"),
}
Delay = commands.get_timedelta_converter(minimum=timedelta(minutes=5), default_unit="minutes")


class ListPageSource(menus.ListPageSource):
    def format_page(self, menu, page):
        return page


class ConfirmMenu(menus.Menu):
    def __init__(
        self, content: Optional[str] = None, embed: Optional[discord.Embed] = None, *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.content = content
        self.embed = embed
        self.result = None

    async def send_initial_message(self, ctx, channel):
        return await channel.send(content=self.content, embed=self.embed)

    @menus.button("\N{WHITE HEAVY CHECK MARK}")
    async def do_confirm(self, _):
        self.result = True
        self.stop()

    @menus.button("\N{CROSS MARK}")
    async def do_deny(self, _):
        self.result = False
        self.stop()

    async def prompt(self, ctx):
        await self.start(ctx, wait=True)
        return self.result


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


class ActivityTypeConverter(commands.Converter):
    async def convert(self, ctx, argument):
        argument = argument.lower()
        if argument not in AVAILABLE_ACTIVITY_TYPES:
            raise commands.ArgParserFailure(
                "game_type",
                argument,
                custom_help=_("Available activity types: {types}").format(
                    types=list(AVAILABLE_ACTIVITY_TYPES)
                ),
            )
        return AVAILABLE_ACTIVITY_TYPES[argument]


@cog_i18n(_)
class RNDActivity(commands.Cog):
    """Randomly set an activity status on a set interval"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=2042511098, force_registration=True)
        self.config.register_global(statuses=[], delay=600)
        self._running_tasks = [self.bot.loop.create_task(self.status_task())]

    def cog_unload(self):
        for task in self._running_tasks:
            task.cancel()

    async def red_delete_data_for_user(self, **_):
        pass  # nothing to delete

    @staticmethod
    def extract_status(data: Union[str, dict]) -> Tuple[int, str]:
        if isinstance(data, str):
            return 0, data
        return data.get("type", 0), data["game"]

    def format_status(self, status: Union[str, dict], shard: int = 0, **kwargs) -> discord.Activity:
        game_type = 0
        url = None
        if isinstance(status, dict):
            game_type: int = status.get("type", 0)
            url: Optional[str] = status.get("url")
            status: str = status.get("game")

        formatted = Placeholders.parse(status, self.bot, shard, **kwargs)
        # noinspection PyArgumentList
        return discord.Activity(name=formatted, url=url, type=discord.ActivityType(game_type))

    async def update_status(self, statuses: List[str]):
        if not statuses:
            return
        status = choice(statuses)
        for shard in self.bot.shards.keys():
            try:
                game = self.format_status(status, shard=shard)
            except KeyError as e:
                log.exception(
                    "Encountered invalid placeholder while attempting to parse status "
                    "#%s, skipping status update.",
                    statuses.index(status) + 1,
                    exc_info=e,
                )
                return
            await self.bot.change_presence(
                activity=game, status=self.bot.guilds[0].me.status, shard_id=shard
            )

    async def status_task(self):
        await self.bot.wait_until_ready()
        while True:
            try:
                await self.update_status(list(await self.config.statuses()))
            except Exception as e:  # pylint:disable=broad-except
                log.exception("Failed to update bot status", exc_info=e)
            await sleep(int(await self.config.delay()))

    ##############################################

    # noinspection PyTypeChecker
    @commands.group()
    @checks.is_owner()
    @checks.bot_in_a_guild()
    # converting the Placeholders class into a usable str is handled by our translation arguments
    # transformer in rndactivity.shared
    @command_format_args(placeholders=Placeholders)
    async def rndactivity(self, ctx: commands.Context):
        """Manage the bot's random activity statuses

        The following placeholders can be used in status strings:

        {placeholders}

        Any invalid placeholders will cause the status to be ignored."""

    @rndactivity.command(name="delay")
    async def rndactivity_delay(self, ctx: commands.Context, *, duration: Delay):
        """How long to wait between status changes

        The duration can be a bare integer amount of minutes, or a more detailed format such
        as `10m`.

        Minimum duration between activity updates is 5 minutes. Default delay is every 10 minutes.
        """
        await self.config.delay.set(duration.total_seconds())
        await ctx.send(
            _(
                "\N{WHITE HEAVY CHECK MARK} Okay; I'll update my status every {duration}."
                " *(this will only take effect after the next status change)*"
            ).format(duration=Humanize(duration))
        )

    @rndactivity.command(name="add", usage="[activity_type=playing] <status>")
    @command_format_args(
        types=Humanize([f"`{x}`" for x in AVAILABLE_ACTIVITY_TYPES.keys()], style="or")
    )
    async def rndactivity_add(
        self, ctx: commands.Context, game_type: Optional[ActivityTypeConverter], *, game: str
    ):
        """Add a random status

        `activity_type` may be any of either {types}. Defaults to `playing`.

        If `activity_type` is `streaming`, the first argument after `streaming`
        must be a Twitch channel URL."""

        stream = None
        if game_type is None:
            game_type = discord.ActivityType.playing
        elif game_type == discord.ActivityType.streaming:
            game = game.split(" ")
            stream, game = game.pop(), " ".join(game)
            if not TWITCH_REGEX.match(stream):
                raise commands.UserFeedbackCheckFailure(
                    _(
                        "Given activity type was `streaming`, but no Twitch stream"
                        " link was provided."
                    )
                )
        activity = {
            "type": game_type.value,
            "game": game,
            "url": stream.strip("<>") if stream else None,
        }

        try:
            self.format_status(activity, error_deprecated=True)
        except KeyError as e:
            await ctx.send(
                warning(
                    _(
                        "Failed to parse the given status: `{placeholder}` is not a valid"
                        " placeholder name"
                    ).format(placeholder=str(e))
                )
            )
        else:
            async with self.config.statuses() as statuses:
                statuses.append(activity)
                await ctx.send(tick(_("Added status **#{id}**").format(id=len(statuses))))

    @rndactivity.command(name="parse")
    async def rndactivity_parse(self, ctx: commands.Context, *, status: str):
        """Parse placeholder arguments in a given string"""
        shard = getattr(ctx.guild, "shard_id", 0)

        try:
            result = self.format_status(status, shard=shard, error_deprecated=True)
        except KeyError as e:
            await ctx.send(
                warning(
                    _(
                        "Failed to parse the given status: `{placeholder}` is not a valid"
                        " placeholder name"
                    ).format(placeholder=str(e))
                )
            )
        else:
            await ctx.send(
                _("\N{INBOX TRAY} **Input:** {input}\n\N{OUTBOX TRAY} **Result:** {result}").format(
                    input=escape(status, mass_mentions=True),
                    result=escape(result.name, mass_mentions=True),
                ),
                allowed_mentions=discord.AllowedMentions(users=False, roles=False, everyone=False),
            )

    @rndactivity.command(name="remove", aliases=["delete"])
    @checks.bot_has_permissions(embed_links=True)
    async def rndactivity_remove(self, ctx: commands.Context, status: int):
        """Remove a given status from the list of statuses to select from"""
        async with self.config.statuses() as statuses:
            if len(statuses) < status:
                await ctx.send(
                    warning(_("No such status with the ID {id} exists").format(id=status))
                )
                return

            removed = statuses.pop(status - 1)
            if not statuses:
                await self.bot.change_presence(
                    activity=None, status=getattr(ctx.me, "status", None)
                )

        activity_type, activity_name = self.extract_status(removed)
        # noinspection PyArgumentList
        removed_activity = f"**{GAME_TYPES[discord.ActivityType(activity_type)]}** {activity_name}"

        await ctx.send(
            tick(_("Removed status #{id}").format(id=status)),
            embed=discord.Embed(description=removed_activity),
        )

    @rndactivity.command(name="list")
    async def rndactivity_list(self, ctx: commands.Context):
        """List all set statuses"""
        statuses = list(await self.config.statuses())
        if not statuses:
            await ctx.send(
                _(
                    "No statuses are currently set! Use `{prefix}rndactivity add` to add some!"
                ).format(prefix=ctx.clean_prefix)
            )
            return

        pages = []
        for status in statuses:
            index = statuses.index(status) + 1
            game_type = 0
            stream_url = None
            if isinstance(status, dict):
                game_type = status.get("type", 0)
                stream_url = status.get("url")
                status = status.get("game")
            status = escape(status, formatting=True, mass_mentions=True)
            if stream_url:
                status = f"[{status}]({stream_url})"
            # noinspection PyArgumentList
            pages.append(
                f"**#{index}** \N{EM DASH} **{GAME_TYPES[discord.ActivityType(game_type)]}**"
                f" {status}"
            )

        colour = await ctx.embed_colour()
        pages = [discord.Embed(colour=colour, description=x) for x in pagify("\n".join(pages))]
        await menus.MenuPages(ListPageSource(pages, per_page=1)).start(ctx)

    @rndactivity.command(name="clear")
    async def rndactivity_clear(self, ctx: commands.Context):
        """Delete all currently set statuses"""
        amount = len(await self.config.statuses())
        if await ConfirmMenu(
            content=_(
                "Are you sure you want to remove"
                " {count, plural, one {# status} other {# statuses}}?\nThis action is irreversible!"
            ).format(count=amount),
            delete_message_after=True,
        ).prompt(ctx):
            await self.config.statuses.set([])
            await self.bot.change_presence(activity=None, status=self.bot.guilds[0].me.status)
            await ctx.send(
                tick(
                    _("Removed {count, plural, one {# status} other {# statuses}}.").format(
                        count=amount
                    )
                )
            )
        else:
            await ctx.send(_("Cancelled.").format())
