from datetime import datetime
from typing import Iterable, List, Optional, Tuple

from babel.dates import format_timedelta
from parse import parse
from redbot.core.bot import Red
from redbot.core.i18n import get_babel_regional_format

from red_icu.util import LazyStr
from rndactivity.shared import icu as _
from rndactivity.shared import log

warned_deprecated = set()


class Placeholder:
    def __init__(
        self,
        func,
        deprecated: bool = False,
        aliases: List[str] = None,
        name: str = None,
        description: Optional[LazyStr] = None,
    ):
        self._func = func
        self.name = func.__name__ if name is None else name
        self.deprecated = deprecated
        self.aliases = aliases or []
        self.description = description or _("*No description provided*")

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<Placeholder name={self.name!r} aliases={self.aliases!r}>"

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)


def placeholder(
    maybe_func=None,
    *,
    name: str = None,
    deprecated: bool = False,
    aliases: List[str] = None,
    description: Optional[LazyStr] = None,
):
    def decorator(func):
        return Placeholder(
            func, deprecated=deprecated, aliases=aliases, name=name, description=description
        )

    if maybe_func:
        return decorator(maybe_func)

    return decorator


# noinspection PyUnusedLocal
class Placeholders:
    def __iter__(self) -> Iterable[Tuple[str, Placeholder]]:
        for k in dir(self):
            v = getattr(self, k)
            if isinstance(v, Placeholder):
                yield k, v

    def __str__(self):
        return "\n".join(
            f"**{{{k}}}** \N{EM DASH} {v.description!s}" for k, v in self if not v.deprecated
        )

    @classmethod
    def parse(cls, status: str, bot: Red, shard: int, error_deprecated: bool = False):
        self = cls()
        placeholders = dict(self)
        keys = {}
        for plc in placeholders.values():
            keys[plc.name] = plc.name
            for alias in plc.aliases:
                keys[alias] = plc.name
        # Doing this also filters out invalid placeholders, and lets us filter down to just
        # invoking the placeholder parsers for what we need.
        args = {k for k in parse(status, status.format(**keys)).named.values()}
        kwargs = {}
        log.debug("Extracted args %r from parse", args)
        for arg in args:
            plc: Placeholder = placeholders[arg]
            if plc.deprecated:
                if error_deprecated:
                    raise KeyError(arg)
                if arg not in warned_deprecated:
                    log.warning(
                        "%r is a deprecated placeholder and will be removed in the near"
                        " future, rendering any strings that contain it unable to be"
                        " chosen to be the bot's activity status.",
                        arg,
                    )
                    warned_deprecated.add(arg)
            val = plc(self, bot, shard)
            kwargs[plc.name] = val
            for alias in plc.aliases:
                kwargs[alias] = val
        return status.format(**kwargs)

    @placeholder(
        aliases=["guilds"],
        description=_("How many servers are on the shard this status displays on"),
    )
    def servers(self, bot: Red, shard: int) -> int:
        return len([x for x in bot.guilds if x.shard_id == shard])

    @placeholder(description=_("The current shard this status is displaying on"))
    def shard(self, bot: Red, shard: int) -> int:
        return shard + 1

    @placeholder(description=_("How many shards the bot currently has"))
    def shards(self, bot: Red, shard: int) -> int:
        return bot.shard_count

    @placeholder(
        aliases=["lavalink", "playing_music"],
        description=_("How many servers are currently playing music with Audio"),
    )
    def music(self, bot: Red, shard: int) -> int:
        import lavalink

        return len(lavalink.active_players())

    @placeholder(description=_("How many members the bot can see"))
    def members(self, bot: Red, shard: int):
        return sum(x.member_count for x in bot.guilds)

    @placeholder(description=_("How many unique users the bot can see"))
    def users(self, bot: Red, shard: int):
        return len(bot.users)

    @placeholder(description=_("How many channels the bot can see"))
    def channels(self, bot: Red, shard: int):
        return sum(len(x.channels) for x in bot.guilds)

    @placeholder(description=_("How long the bot has been online for"))
    def uptime(self, bot: Red, shard: int):
        return format_timedelta(
            datetime.utcnow() - bot.uptime, locale=get_babel_regional_format(), format="narrow"
        )
