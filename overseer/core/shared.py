import logging
import os
from pathlib import Path
from typing import Any, Dict

from redbot.core import Config, VersionInfo

from swift_i18n.red import Translator

__all__ = ("translate", "config", "rebuild_defaults", "log", "version_info")
version_info = VersionInfo.from_str("1.0.0")
log = logging.getLogger("red.overseer")
translate = Translator(str(Path(__file__).parent))
if "BUILDING_DOCS" not in os.environ:
    try:
        config = Config.get_conf(None, cog_name="Overseer", identifier=2401248235421)
    except RuntimeError:
        config = None
else:
    config = None


def rebuild_defaults() -> None:
    """Collect module defaults and setup the cog's Config defaults to match"""

    from overseer.core.module import modules
    from overseer.utils import unflatten_dict

    defaults: Dict[str, Dict[str, Any]] = {}
    for mod in modules.values():
        if mod.config_scope not in defaults:
            defaults[mod.config_scope] = {}

        mod_defaults = {
            **unflatten_dict({x.key: x.default for x in mod().loggers}),
            "_log_channel": None,
        }
        defaults[mod.config_scope][mod.id] = mod_defaults

    defaults["guild"] = {**defaults.get("guild", {}), "_responsible_mod": False}
    for scope, values in defaults.items():
        if config:
            config.register_custom(scope, **values)
