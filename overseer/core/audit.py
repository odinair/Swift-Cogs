from __future__ import annotations

from collections import namedtuple

from contextvars import ContextVar
from functools import partial
from typing import Callable, Awaitable, Union, Optional, List

import discord

from overseer.utils import undefined

# noinspection PyProtectedMember
from overseer.core.logger import ARGUMENTS, INSTANCE
from overseer.core.module import Module

__all__ = ("AuditLogHandler", "audit", "DummyMember")


class DummyMember(namedtuple("Member", ["id", "mention"])):
    def __bool__(self):
        return False

    def __eq__(self, other):
        return False


DummyMember = DummyMember(id=None, mention="`<unknown user>`")
RESPONSIBLE: ContextVar[Optional[discord.Member]] = ContextVar("responsible", default=undefined)

# FIXME: The way this is implemented works as intended, but makes pylint complain like all hell
#        with an avalanche of no-value-for-parameter messages


def audit(action: Union[discord.AuditLogAction, List[discord.AuditLogAction]]):
    """Retrieve a :class:`AuditLogHandler` with a wrapped check method

    .. important::
        The decorated method will always be given the Module instance for the current context
        (``self`` in the below example), even if it isn't in the Module class or if
        it's a :class:`classmethod` or :class:`staticmethod`.

    Example
    -------
    .. code-block:: python

        class MyModule(Module):
            @audit(discord.AuditLogAction.kick)
            def audit(self, args: tuple, kwargs: dict, entry: discord.AuditLogEntry):
                return entry.target.id == args[0].id

            @event("kick")
            async def kick(self, member: discord.Member):
                # will be either a discord.Member object or DummyMember
                # even if your attached check method is not async, this must be awaited!
                responsible = await self.audit()
    """

    def decorator(func: Callable[..., Union[bool, Awaitable[bool]]]) -> AuditLogHandler:
        return AuditLogHandler(action, func)

    return decorator


class AuditLogHandler:
    """Caching layer around :func:`overseer.core.module.Module.responsible_mod`

    If this class has no attached check method, the first audit log entry will always be assumed
    to be correct.

    .. seealso::
        * :func:`audit`

    Attributes
    ---------
    type: Union[discord.AuditLogAction, List[discord.AuditLogAction]]
        One or more audit log entry types to filter to when retrieving entries
    check: Optional[Callable[[Module, tuple, dict], Union[bool, Awaitable[bool]]]]
        Method to call when
    """

    def __init__(
        self, action: Union[discord.AuditLogAction, List[discord.AuditLogAction]], check: Callable
    ):
        self.type = action
        self.check: Callable = check

    async def __call__(self):
        """Retrieve audit log information for the current context

        If no member can be found for the current context, an object with an ``id`` attribute
        containing the value :obj:`None` is returned for convenience. This object will still
        resolve to :obj:`False` in ``if`` checks.
        """
        module: Module = INSTANCE.get()
        responsible = RESPONSIBLE.get()
        if responsible is not undefined:
            return responsible
        args, kwargs = ARGUMENTS.get()
        check = self.check or (lambda *_: True)
        responsible = (
            await module.responsible_moderator(self.type, partial(check, module, args, kwargs))
            or DummyMember
        )
        RESPONSIBLE.set(responsible)
        return responsible
