from difflib import unified_diff
from typing import List, Sequence

import discord
from redbot.core.utils.chat_formatting import box

from overseer.core import event, Module, translate as translate_

translate = translate_.group("message")


class Message(
    Module,
    id="message",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
):
    @event("edit", description=translate.lazy("events.edit"))
    def edit(self, before: discord.Message, after: discord.Message):
        if after.author.bot or before.content == after.content:
            return

        return "\n".join(
            [
                translate(
                    "edited",
                    mid=after.id,
                    author=after.author.mention,
                    id=after.author.id,
                    channel=after.channel.mention,
                ),
                box(
                    "\n".join(
                        unified_diff(
                            before.content.replace("`", "`\N{ZERO WIDTH JOINER}").split("\n"),
                            after.content.replace("`", "`\N{ZERO WIDTH JOINER}").split("\n"),
                            lineterm="",
                            fromfile=translate.root("before"),
                            tofile=translate.root("after"),
                        )
                    ),
                    lang="diff",
                ),
            ]
        ),

    @event("delete", description=translate.lazy("events.delete"))
    def delete(self, message: discord.Message):
        # only ignore bots if it's a standard message; if it's a system message such
        # as pinning a message, it's likely worth logging.
        if message.type == discord.MessageType.default and message.author.bot:
            return

        embed = discord.Embed(
            colour=discord.Colour.red(),
            title=translate("message_content"),
            description=(
                message.content
                if message.type == discord.MessageType.default
                else message.system_content
            )
            or discord.Embed.Empty,
            timestamp=message.created_at,
        )

        if message.type != discord.MessageType.default:
            # special-case system messages, as these almost always don't have much
            # useful information we can log beyond their content.
            # the system message content currently isn't translated, but I'm not worried
            # about handling that currently.
            return {
                "content": translate("sys_deleted", channel=message.channel.mention),
                "embed": embed,
            }

        if message.attachments:
            embed.add_field(
                name=translate("attachments"),
                value="\n".join(f"<{x.url}>" for x in message.attachments),
                inline=False,
            )

        return {
            "content": translate(
                "deleted",
                mid=message.id,
                author=message.author.mention,
                id=message.author.id,
                channel=message.channel.mention,
            ),
            "embed": embed,
        }

    @event("filtered", description=translate.lazy("events.filtered"))
    def filtered(self, message: discord.Message, hits: Sequence[str]):
        if message.author.bot:
            return

        embed = discord.Embed(
            colour=discord.Colour.red(),
            title=translate("message_content"),
            description=message.content,
        ).add_field(name=translate("filtered_terms"), value="\n".join(hits))

        if message.attachments:
            embed.add_field(
                name=translate("attachments"),
                value="\n".join(f"<{x.url}>" for x in message.attachments),
                inline=False,
            )

        return {
            "content": translate(
                "filtered",
                mid=message.id,
                author=message.author.mention,
                id=message.author.id,
                channel=message.channel.mention,
            ),
            "embed": embed,
        }

    @event("bulkdelete", description=translate.lazy("events.bulk"))
    def bulk_delete(self, channel: discord.TextChannel, message_ids: List[int]):
        if not message_ids:
            return

        return translate("deleted_bulk", num=len(message_ids), channel=channel.mention)
