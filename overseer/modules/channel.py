from difflib import unified_diff
from typing import Tuple

import discord
from babel.units import format_compound_unit
from redbot.core.utils.chat_formatting import bold, box

from overseer.core import event, Module, translate as translate_, audit
from overseer.utils import format_permission
from swift_i18n import Humanize

translate = translate_.group("channel")


class Channel(
    Module,
    id="channel",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
):
    @audit(
        [
            discord.AuditLogAction.channel_create,
            discord.AuditLogAction.channel_delete,
            discord.AuditLogAction.channel_update,
            discord.AuditLogAction.overwrite_create,
            discord.AuditLogAction.overwrite_delete,
            discord.AuditLogAction.overwrite_update,
        ]
    )
    def audit(self, args: Tuple[discord.abc.GuildChannel, ...], _, entry: discord.AuditLogEntry):
        return args[0].id == entry.target.id

    @event("create", description=translate.lazy("events.create"))
    async def create(self, channel: discord.abc.GuildChannel):
        responsible = await self.audit()
        return translate(
            "created" if not responsible else "created_responsible",
            channel=channel.mention,
            id=channel.id,
            moderator=responsible.mention,
            mod_id=responsible.id,
        )

    @event("delete", description=translate.lazy("events.delete"))
    async def delete(self, channel: discord.abc.GuildChannel):
        responsible = await self.audit()
        return translate(
            "deleted" if not responsible else "deleted_responsible",
            channel=channel.name,
            id=channel.id,
            moderator=responsible.mention,
            mod_id=responsible.id,
        )

    @event("update")
    def update(self):
        pass

    @update.event("name", description=translate.lazy("events.update.name"))
    async def _update_name(self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
        if before.name != after.name:
            responsible = await self.audit()
            return translate(
                "name_changed" if not responsible else "name_changed_responsible",
                mention=before.mention,
                id=before.id,
                before=before.name,
                after=after.name,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("category", description=translate.lazy("events.update.category"))
    def _update_category(self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
        if before.category != after.category:
            return translate(
                "category_changed" if after.category else "category_uncategorized",
                mention=before.mention,
                id=before.id,
                category=getattr(after.category, "mention", None),
                cat_id=getattr(after.category, "id", None),
            )

    @update.event("position", description=translate.lazy("events.update.position"))
    def _update_position(self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
        if before.position != after.position:
            return translate(
                "position_changed",
                mention=before.mention,
                id=before.id,
                before=before.position,
                after=after.position,
            )

    # lol, i got to make my own permission overwrite logging because discord's
    # own audit logs don't even work properly, what an absolute fucking joke.
    # https://u.odinair.xyz/Oc0O3toJ.png
    @update.event("overwrites", description=translate.lazy("events.update.overwrites"))
    async def _update_overwrites(
        self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel
    ):
        # This is an interesting event due to how categories work. The API doesn't
        # expose a way to easily determine if a channel is syncing its
        # overwrites with the category it's contained in, which makes this
        # much more susceptible to getting ourselves rate limited. It's very likely we could
        # determine it through some dumb shit, but honestly, it's just not
        # worth dealing with currently.

        b = before.overwrites.copy()
        a = after.overwrites.copy()
        b.update({x: discord.PermissionOverwrite() for x in a.keys() if x not in b})
        a.update({x: discord.PermissionOverwrite() for x in b.keys() if x not in a})
        changes = {
            r: {x: z for x, z in o if getattr(b[r], x, None) is not z}
            for r, o in a.items()
            if b[r] != o
        }

        if not changes:
            return

        responsible = await self.audit()
        for k, v in changes.items():
            changed_for_k = [
                translate.root(
                    (
                        "item_cleared"
                        if nv is None
                        else "item_granted"
                        if nv is True
                        else "item_revoked"
                    ),
                    item=bold(format_permission(p)),
                )
                for p, nv in v.items()
            ]

            if changed_for_k:
                yield {
                    "content": translate(
                        "overwrites_changed"
                        if not responsible
                        else "overwrites_changed_responsible",
                        type="role" if isinstance(k, discord.Role) else "member",
                        mention=before.mention,
                        id=before.id,
                        item_id=k.id,
                        item=k.mention,
                        moderator=responsible.mention,
                        mod_id=responsible.id,
                    ),
                    "embed": discord.Embed(description="\n".join(changed_for_k)),
                }

    @update.event("topic", description=translate.lazy("events.update.topic"))
    async def _update_topic(
        self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel
    ):
        if not isinstance(before, discord.TextChannel):
            return

        if (before.topic or "") != (after.topic or ""):
            responsible = await self.audit()
            return "\n".join(
                [
                    translate(
                        "topic_changed" if not responsible else "topic_changed_responsible",
                        mention=before.mention,
                        id=before.id,
                        moderator=responsible.mention,
                        mod_id=responsible.id,
                    ),
                    box(
                        "\n".join(
                            unified_diff(
                                (before.topic.split("\n") if before.topic else []),
                                (after.topic.split("\n") if after.topic else []),
                                lineterm="",
                                fromfile=translate.root("before"),
                                tofile=translate.root("after"),
                            )
                        ),
                        lang="diff",
                    ),
                ]
            )

    @update.event("nsfw", description=translate.lazy("events.update.nsfw"))
    async def _update_nsfw(self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
        if not isinstance(before, discord.TextChannel):
            return

        if before.nsfw != after.nsfw:
            responsible = await self.audit()
            return translate(
                ("now_nsfw" if after.nsfw else "no_longer_nsfw")
                + ("_responsible" if responsible else ""),
                mention=before.mention,
                id=before.id,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("slowmode", description=translate.lazy("events.update.slowmode"))
    async def _update_slow(self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
        if not isinstance(before, discord.TextChannel):
            return

        if before.slowmode_delay != after.slowmode_delay:
            responsible = await self.audit()
            return translate(
                "slowmode_changed" if not responsible else "slowmode_changed_responsible",
                mention=before.mention,
                id=before.id,
                slow=after.slowmode_delay,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("userlimit", description=translate.lazy("events.update.user_limit"))
    def _update_userlimit(self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
        if not isinstance(before, discord.VoiceChannel):
            return

        if before.user_limit != after.user_limit:
            # you'd think this would be in the audit log - but it isn't. :meowshrug:
            return translate(
                "user_limit_changed",
                mention=before.mention,
                id=before.id,
                before=before.user_limit,
                after=after.user_limit,
            )

    @update.event("bitrate", description=translate.lazy("events.update.bitrate"))
    async def _update_bitrate(
        self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel
    ):
        if not isinstance(before, discord.VoiceChannel):
            return

        if before.bitrate != after.bitrate:
            responsible = await self.audit()
            return translate(
                "bitrate_changed" if not responsible else "bitrate_changed_responsible",
                mention=before.mention,
                id=before.id,
                before=Humanize(
                    format_compound_unit,
                    int(str(before.bitrate)[:-3]),
                    "kilobit",
                    denominator_unit="second",
                ),
                after=Humanize(
                    format_compound_unit,
                    int(str(after.bitrate)[:-3]),
                    "kilobit",
                    denominator_unit="second",
                ),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )
