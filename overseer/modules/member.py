import asyncio

import discord
from redbot.core.utils.chat_formatting import escape

from overseer.core import event, Module, translate as translate_, audit

translate = translate_.group("member")


class Member(
    Module,
    id="member",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
):
    @audit([discord.AuditLogAction.member_update, discord.AuditLogAction.member_role_update])
    def audit(self, args, _, entry: discord.AuditLogEntry):
        return args[0].id == entry.target.id

    @event("join", description=translate.lazy("events.join"))
    def join(self, member: discord.Member):
        return translate("join", member=member.mention, id=member.id)

    @event("leave", description=translate.lazy("events.leave"))
    def leave(self, member: discord.Member):
        return translate("leave", member=member.mention, id=member.id)

    @event("update")
    def update(self):
        pass

    @update.event("name", description=translate.lazy("events.update.name"))
    def _update_name(self, before: discord.Member, after: discord.Member):
        if before.name != after.name:
            return translate(
                "name",
                before=escape(str(before), formatting=True),
                after=escape(str(after), formatting=True),
                member=after.mention,
                id=after.id,
            )

    @update.event("nickname", description=translate.lazy("events.update.nick"))
    async def _update_nick(self, before: discord.Member, after: discord.Member):
        if before.nick != after.nick:
            responsible = await self.audit()
            return translate(
                "nick"
                if not responsible
                else ("nick_responsible" if responsible != after else "nick_self"),
                member=after.mention,
                id=after.id,
                nick=escape(after.nick or translate("none"), formatting=True),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("roles", description=translate.lazy("events.update.roles"))
    async def _update_roles(self, before: discord.Member, after: discord.Member):
        # Sleep before doing anything to allow discord.py to remove roles that are
        # being deleted from the guild's role list
        await asyncio.sleep(0.3)

        # Filter out integration roles such as Twitch subscriber or Nitro booster roles
        # Maybe add the ability to log these roles in a different event in the future?
        ar = [x for x in after.roles if not x.managed]
        # Additionally filter out roles that are being deleted, since this can cause
        # an extreme amount of noise with roles that were assigned to a sizable amount
        # of members
        br = [x for x in before.roles if not x.managed and x in after.guild.roles]

        if br != ar:
            changes = {
                r: True if r not in before.roles else False
                for r in {*br, *ar}
                if not (r in ar and r in br)
            }
            responsible = await self.audit()
            # Ignore roles that we're adding to avoid spamming logs with
            # things such as reaction roles or the like.
            if responsible == self.bot.user:
                return

            return {
                "content": translate(
                    "roles"
                    if not responsible
                    else ("roles_responsible" if responsible != after else "roles_self"),
                    member=after.mention,
                    id=after.id,
                    moderator=responsible.mention,
                    mod_id=responsible.id,
                ),
                "embed": discord.Embed(
                    description="\n".join(
                        [
                            translate.root("item_granted" if v else "item_revoked", item=k.mention)
                            for k, v in changes.items()
                            if v is not None
                        ]
                    )
                ),
            }
