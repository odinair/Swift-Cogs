import discord
from redbot.core.utils.chat_formatting import bold, escape

from overseer.core import event, Module, translate as translate_, audit
from overseer.core.shared import log
from overseer.utils import format_permission

translate = translate_.group("role")


class Role(
    Module,
    id="role",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
):
    @audit(
        [
            discord.AuditLogAction.role_update,
            discord.AuditLogAction.role_create,
            discord.AuditLogAction.role_delete,
        ]
    )
    def audit(self, args, _, entry: discord.AuditLogEntry):
        return entry.target.id == args[0].id

    @event("create", description=translate.lazy("events.create"))
    async def create(self, role: discord.Role):
        responsible = await self.audit()
        return {
            "content": translate(
                "created" if not responsible else "created_responsible",
                name=role.mention if not role.is_default() else role.name,
                id=role.id,
                moderator=responsible.mention,
                mod_id=responsible.id,
            ),
            "embed": discord.Embed()
            .add_field(
                name=translate("hoisted"), value=translate.root("yes" if role.hoist else "no")
            )
            .add_field(
                name=translate("mentionable"),
                value=translate.root("yes" if role.mentionable else "no"),
            )
            .add_field(
                name=translate("colour"),
                value=str(role.colour) if role.colour.value != 0 else translate.root("none"),
            )
            .add_field(
                name=translate("permissions"),
                value=", ".join(
                    [format_permission(x) for x, y in role.permissions if y]
                    or [translate.root("none")]
                ),
                inline=False,
            ),
        }

    @event("delete", description=translate.lazy("events.delete"))
    async def delete(self, role: discord.Role):
        responsible = await self.audit()
        return translate(
            "deleted" if not responsible else "deleted_responsible",
            name=escape(role.name, mass_mentions=True, formatting=True),
            id=role.id,
            moderator=responsible.mention,
            mod_id=responsible.id,
        )

    @event("update")
    def update(self):
        pass

    @update.event("name", description=translate.lazy("events.update.name"))
    async def _update_name(self, before: discord.Role, after: discord.Role):
        if before.name != after.name:
            responsible = await self.audit()
            return translate(
                "name_changed" if not responsible else "name_changed_responsible",
                before=escape(before.name, mass_mentions=True, formatting=True),
                name=after.mention if not after.is_default() else after.name,
                id=after.id,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("permissions", description=translate.lazy("events.update.permissions"))
    async def _update_permissions(self, before: discord.Role, after: discord.Role):
        if before.permissions.value != after.permissions.value:
            changed = {
                k: (
                    None
                    if getattr(before.permissions, k) is getattr(after.permissions, k)
                    else getattr(after.permissions, k)
                )
                for k, _ in discord.Permissions()
            }

            if not any([v is not None for v in changed.values()]):
                log.warning(
                    "Permissions value changed from %s to %s, but no permissions were changed?",
                    before.permissions.value,
                    after.permissions.value,
                )
                return

            responsible = await self.audit()
            return {
                "content": translate(
                    "permissions_changed" if not responsible else "permissions_changed_responsible",
                    name=after.mention if not after.is_default() else after.name,
                    id=after.id,
                    moderator=responsible.mention,
                    mod_id=responsible.id,
                ),
                "embed": discord.Embed(
                    description="\n".join(
                        [
                            translate.root(
                                "item_granted" if v else "item_revoked",
                                item=bold(format_permission(k)),
                            )
                            for k, v in changed.items()
                            if v is not None
                        ]
                    )
                ),
            }

    @update.event("colour", description=translate.lazy("events.update.colour"))
    async def _update_colour(self, before: discord.Role, after: discord.Role):
        if before.colour != after.colour:
            responsible = await self.audit()
            return translate(
                "colour_changed" if not responsible else "colour_changed_responsible",
                name=after.mention if not after.is_default() else after.name,
                id=after.id,
                before=str(before.colour) if before.colour.value != 0 else translate.root("none"),
                after=str(after.colour) if after.colour.value != 0 else translate.root("none"),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("mentionable", description=translate.lazy("events.update.mentionable"))
    async def _update_mentionable(self, before: discord.Role, after: discord.Role):
        if before.mentionable != after.mentionable:
            responsible = await self.audit()
            return translate(
                ("now_mentionable" if after.mentionable else "not_mentionable")
                + ("_responsible" if responsible else ""),
                name=after.mention if not after.is_default() else after.name,
                id=after.id,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("hoist", description=translate.lazy("events.update.hoist"))
    async def _update_hoist(self, before: discord.Role, after: discord.Role):
        if before.hoist != after.hoist:
            responsible = await self.audit()
            return translate(
                ("now_hoisted" if after.hoist else "not_hoisted")
                + ("_responsible" if responsible else ""),
                name=after.mention if not after.is_default() else after.name,
                id=after.id,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("position", description=translate.lazy("events.update.position"))
    def _update_position(self, before: discord.Role, after: discord.Role):
        if before.position != after.position:
            return translate(
                "position_changed",
                name=after.mention if not after.is_default() else after.name,
                id=after.id,
                before=before.position,
                after=after.position,
            )
