import discord

from overseer.core import ghost, Module, translate as translate_

translate = translate_.group("voice")


class Voice(
    Module,
    id="voice",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
):
    update = ghost()

    @update.event("channel", description=translate.lazy("events.channel"))
    def channel(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if before.channel != after.channel:
            return translate(
                "channel",
                (
                    "join"
                    if before.channel is None
                    else "leave"
                    if after.channel is None
                    else "switch"
                ),
                before=getattr(before.channel, "mention", None),
                after=getattr(after.channel, "mention", None),
                aid=getattr(after.channel, "id", None),
                bid=getattr(before.channel, "id", None),
                member=member,
                id=member.id,
            )

    @update.event("mute")
    def mute(self):
        pass

    @mute.event("self", description=translate.lazy("events.mute.self"))
    def _mute_self(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if before.self_mute != after.self_mute:
            return translate(
                "mute" if after.self_mute else "unmute", member=member, id=member.id, type="self"
            )

    @mute.event("server", description=translate.lazy("events.mute.server"))
    def _mute_server(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if before.mute != after.mute:
            return translate(
                "mute" if after.mute else "unmute", member=member, id=member.id, type="server"
            )

    @update.event("deaf")
    def deaf(self):
        pass

    @deaf.event("self", description=translate.lazy("events.deaf.self"))
    def _deaf_self(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if before.self_deaf != after.self_deaf:
            return translate(
                "deaf" if after.self_deaf else "undeaf", member=member, id=member.id, type="self"
            )

    @deaf.event("server", description=translate.lazy("events.deaf.server"))
    def _deaf_server(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if before.deaf != after.deaf:
            return translate(
                "deaf" if after.deaf else "undeaf", member=member, id=member.id, type="server"
            )
