from typing import MutableMapping, Any, Sequence, Union, Mapping, Callable, Dict


# noinspection PyPep8Naming
import discord

from swift_i18n.util import LazyStr
from overseer.core.shared import translate

permission_translations: Dict[str, LazyStr] = {}

for _k, _ in discord.Permissions():
    permission_translations[_k] = translate.lazy(
        "permissions", _k, default=_k.replace("_", " ").title()
    )


class undefined:
    def __repr__(self):
        return f"<{self.__module__}.undefined>"

    def __bool__(self):
        return False


undefined = undefined()


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


def flatten_dict(dct: MutableMapping[str, Any], parent_key: str = "", sep: str = "."):
    # originally from https://stackoverflow.com/a/6027615
    for k, v in dct.items():
        new_key = parent_key + sep + str(k) if parent_key else str(k)
        if isinstance(v, MutableMapping):
            yield from flatten_dict(v, new_key, sep=sep)
        else:
            yield (new_key, v)


def flatten_list(lst: Sequence[Any]):
    for item in lst:
        if isinstance(item, (list, tuple, set)):
            yield from flatten_list(item)
        else:
            yield item


def flatten(to_flatten: Union[MutableMapping, Sequence], sep: str = ".") -> Union[dict, list]:
    return (
        dict(flatten_dict(to_flatten, sep=sep))
        if isinstance(to_flatten, MutableMapping)
        else list(flatten_list(to_flatten))
    )


def unflatten_dict(d: Dict[str, Any], sep: str = ".") -> dict:
    new = {}

    for k, v in d.items():
        k = k.split(sep)
        new_ = new
        for x in k[:-1]:
            if x not in new_:
                new_[x] = {}
            new_ = new_[x]
        new_[k[-1]] = v

    return new


def deep_merge(source: dict, destination: dict, *, merge_lists: Union[bool, Sequence[str]] = False):
    # original: https://stackoverflow.com/a/20666342
    for key, value in source.items():
        if isinstance(value, dict):
            node = destination.setdefault(key, {})
            deep_merge(value, node, merge_lists=merge_lists)
        elif (
            merge_lists
            and isinstance(value, list)
            and key in destination
            and isinstance(destination[key], list)
            and (not isinstance(merge_lists, Sequence) or key in merge_lists)
        ):
            for item in value:
                if item in destination[key]:
                    continue
                destination[key].append(item)
        else:
            destination[key] = value

    return destination


def replace_dict_values(
    dct: Mapping, replace_with=undefined, converter: Callable[[Any], Any] = undefined
) -> dict:
    if replace_with is undefined and converter is None:
        raise RuntimeError("Expected either `value` or `converter` to be passed, got neither")

    return {
        k: replace_dict_values(v, converter=converter, replace_with=replace_with)
        if isinstance(v, Mapping)
        else converter(v)
        if converter
        else replace_with
        for k, v in dct.items()
    }


def format_permission(perm: str):
    return str(permission_translations.get(perm, perm.replace("_", " ").title()))
