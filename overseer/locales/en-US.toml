continued = '{module} (continued)'
item_cleared = '{item} \N{EM DASH} cleared'
item_granted = '{item} \N{EM DASH} granted'
item_revoked = '{item} \N{EM DASH} revoked'
page = 'Page {current}/{total}'
version = 'Overseer v{version}'
no_description = 'No description set'
usage_info = '''To toggle settings: `{prefix}overseer <module.event[=<true/false/channel>]> [...]`
To include responsible moderator in logging: `{prefix}overseer audit`'''
channel_bool_convert_fail = 'Failed to convert `{item}` to either a text channel or boolean'
overseer_settings = 'Overseer Settings'
expected_setting = 'Expected at least one module setting following the module name, got none'
no_settings_changed = "I couldn't update any of the settings you specified. Sorry."
settings_updated = 'The following settings have been updated:'
setting_enabled = '\N{SPEAKER WITH ONE SOUND WAVE} `{setting}` events will now be logged'
setting_disabled = '\N{SPEAKER WITH CANCELLATION STROKE} `{setting}` events will no longer be logged'
setting_enabled_override = '\N{SPEAKER WITH ONE SOUND WAVE} `{setting}` events will now be logged to channel {channel}'
reset_confirmation = 'Are you sure you want to reset settings for the **{module}** module? **This action is irreversible!**'
reset_server_confirmation = "Are you sure you want to reset this server's log settings? **This action is irreversible!**"
reset_success = 'Settings have been reset.'
channel_no_permissions = "I'm not able to send messages in that channel."
set_log_channel = 'The {n, plural, =1 {module} other {modules}} {mods} will now log to channel {channel}'
cleared_log_channel = 'The log channel for {mods} {n, plural, =1 {has been cleared} other {have been cleared}}'
no_mod_description = 'No module description was provided'
no_such_module = 'Either no such module `{module}` exists, or you lack the necessary permissions to modify its settings.'
mod_desc = '**Description** \N{EM DASH} {description}'
diff = 'Content difference'
ok_then = 'Okay then.'
yes = 'Yes'
no = 'No'
none = 'None'
unknown = 'Unknown'
before = "Before"
after = "After"
invalid_setting_key = 'Module `{module}` does not have an event named `{event}`'
audit_enabled = '''Event logging will now include audit log information.

\N{WARNING SIGN} Not all events include audit log information, either due to Discord limitations \
or due to other concerns with including such information in logging.
'''
audit_disabled = 'Event logging will no longer include audit log information.'

# TODO: this should really be simplified / made to not be a giant wall of text
overseer_explanation = '''All of Overseer's logging is split into logical chunks, which are referred to as modules. \
These modules are comprised of one or more events which can be enabled or disabled individually. This means that if you'd \
like to only know when a role's permissions are changed, but not for example log when its colour or position in the \
role hierarchy is changed, all you need to do is enable the `role.update.permissions` event.

The module ID is always the first portion of the ID you see in the output of `{prefix}overseer`, which means `role.create` \
is an event named `create` belonging to a module with the ID `role`, which can be used with `{prefix}overseer destination` \
to set a module-level destination.

To toggle events, it's as simple as passing the relevant event ID in the list you see when `{prefix}overseer` is invoked \
without any arguments, such as `{prefix}overseer message.edit` to toggle the `edit` event from the Message module.

You can specifically enable or disable an event by specifying a boolean value after the event ID with an equals (`=`) sign, \
such as `role.create=true` to enable the `create` event from the Role module, or `role.create=false` to similarly disable it.

Additionally, events can be set to override their log destination channel instead of logging to the module-level destination channel \
with the same syntax as above, but with a channel name or ID in place of a boolean value, such as `server.emoji=emoji-changelog` \
to send `emoji` events from the Server module to the `emoji-changelog` channel; this also allows for enabling an event \
from a module without setting the log destination channel for the entire module it belongs to.
'''

[permissions]
  add_reactions = "Add Reactions"
  administrator = "Administrator"
  attach_files = "Attach Files"
  ban_members = "Ban Members"
  change_nickname = "Change Nickname"
  connect = "Connect"
  create_instant_invite = "Create Instant Invite"
  deafen_members = "Deafen Members"
  embed_links = "Embed Links"
  external_emojis = "External Emojis"
  kick_members = "Kick Members"
  manage_channels = "Manage Channels"
  manage_emojis = "Manage Emojis"
  manage_guild = "Manage Server"
  manage_messages = "Manage Messages"
  manage_nicknames = "Manage Nicknames"
  manage_roles = "Manage Roles"
  manage_webhooks = "Manage Webhooks"
  mention_everyone = "Mention Everyone"
  move_members = "Move Members"
  mute_members = "Mute Members"
  read_message_history = "Read Message History"
  read_messages = "Read Messages"
  send_messages = "Send Messages"
  send_tts_messages = "Send TTS Messages"
  speak = "Speak"
  use_voice_activation = "Use Voice Activation"
  view_audit_log = "View Audit Log"
  priority_speaker = "Priority Speaker"
  stream = "Video"

[module_status]
  disabled = '\N{SPEAKER WITH CANCELLATION STROKE} **{module}.{logger}** \N{EM DASH} {description}'
  enabled = '\N{SPEAKER WITH ONE SOUND WAVE} **{module}.{logger}** \N{EM DASH} {description}'
  override_channel = '\N{SPEAKER WITH ONE SOUND WAVE} **{module}.{logger}** (logging to {channel}) \N{EM DASH} {description}'

[logging_to]
  channel = '**Logging to** — {dest}'
  nowhere = '**Logging to** — nowhere (set with `{prefix}overseer destination {module} <channel>`)'

[help]
  cog_class = 'Log anything and everything that happens in your server'
  reset = 'Reset settings for the server or a single module'
  root = 'Manage Overseer event logging'
  explain = 'Explain the basics of how Overseer works'
  channel = '''Set the log channel for a module

  Not passing a channel will stop all further logging for the specified module.

  Events that are set to override this setting will still log to that channel, and must be updated seperately.'''
  audit = '''Toggle including audit log information in log messages

  **The following events do not support this setting due to Discord limitations:**

  - `channel.position`, `channel.category`, `channel.user_limit`

  **The following events do not support this setting due to various concerns with including such information:**

  - All Voice events
  - All Message events
  - `server.emojis`
  '''


[bot]
  __name__ = 'Bot'
  __description__ = 'Global bot-level logging'

  command_exception = '\N{BUG} An unexpected exception occured in command `{qualified}`'
  started = '\N{WHITE HEAVY CHECK MARK} Bot has started up successfully'

  [bot.events]
    command_errors = 'Errors during command execution'
    startup = 'Bot startup completion'


[guild]
  __name__ = 'Server'
  __description__ = 'Server update logging'

  ownership_transferred = '\N{KEY} **Server ownership has been transferred** from **{before}** (`{before_id}`) to **{after}** (`{after_id}`)'

  server_name = '\N{LABEL} Server name has been changed to `{after}`'
  server_name_responsible = '\N{LABEL} {moderator} (`{mod_id}`) changed the server name to `{after}`'

  region_changed = '\N{EARTH GLOBE AMERICAS} Server voice region has changed to {after}'
  region_changed_responsible = '\N{EARTH GLOBE AMERICAS} {moderator} (`{mod_id}`) changed the server voice region to {after}'

  verification_level_set = '\N{ALIEN MONSTER} Server verification level has been changed to **{after}**'
  verification_level_set_responsible = '\N{ALIEN MONSTER} {moderator} (`{mod_id}`) changed the server verification level to **{after}**'

  mfa_enabled = '\N{CLOSED LOCK WITH KEY} Two-factor authentication is now required for moderators'
  mfa_enabled_responsible = '\N{CLOSED LOCK WITH KEY} {moderator} (`{mod_id}`) enabled two-factor authentication requirement for moderators'
  mfa_disabled = '\N{OPEN LOCK} Two-factor authentication is no longer required for moderators'
  mfa_disabled_responsible = '\N{OPEN LOCK} {moderator} (`{mod_id}`) disabled two-factor authentication requirement for moderators'

  emoji_added = '\N{WHITE HEAVY CHECK MARK} Emoji {emoji} `:{name}:` has been added'
  emoji_removed = '\N{PUT LITTER IN ITS PLACE SYMBOL} Emoji `:{emoji}:` has been removed'
  emoji_renamed = '\N{PENCIL} Emoji {emoji} renamed \N{EM DASH} `:{before}:` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} `:{after}:`'

  afk_timeout = '\N{STOPWATCH} AFK timeout changed to {after}'
  afk_timeout_responsible = '\N{STOPWATCH} {moderator} (`{mod_id}`) changed the AFK timeout to {after}'
  afk_channel_changed = '\N{SLEEPING SYMBOL} AFK channel has been changed to {channel} (`{id}`)'
  afk_channel_changed_responsible = '''\N{SLEEPING SYMBOL} {moderator} (`{mod_id}`) changed the server's AFK channel to {channel} (`{id}`)'''
  afk_channel_removed = '\N{SLEEPING SYMBOL} AFK channel has been unset'
  afk_channel_removed_responsible = '''\N{SLEEPING SYMBOL} {moderator} (`{mod_id}`) unset the server's AFK channel'''

  content_filter = '''\N{FRAME WITH PICTURE} Server content filter has been {status, select,
       disabled {disabled}
        no_role {set to scan images from members without any roles}
    all_members {set to scan images from all members}
  }'''
  content_filter_responsible = '''\N{FRAME WITH PICTURE} **{moderator}** (`{mod_id}`) {status, select,
       disabled {disabled the server's content filter}
        no_role {set the server's content filter to scan images from members without any roles}
    all_members {set the server's content filter to scan images from all members}
  }'''

  verification_levels = [
    'None',
    'Low',
    'Medium',
    '(╯°□°）╯︵ ┻━┻',
    '┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻'
  ]

  [guild.voice_regions]
    us-west = 'US West'
    us-east = 'US East'
    us-south = 'US South'
    us-central = 'US Central'
    eu-west = 'Western Europe'
    eu-central = 'Central Europe'
    singapore = 'Singapore'
    london = 'London'
    sydney = 'Sydney'
    amsterdam = 'Amsterdam'
    frankfurt = 'Frankfurt'
    brazil = 'Brazil'
    hongkong = 'Hong Kong'
    russia = 'Russia'
    japan = 'Japan'
    southafrica = 'South Africa'
    vip-us-east = 'VIP US East'
    vip-us-west = 'VIP US West'
    vip-amsterdam = 'VIP Amsterdam'

  [guild.events]
    name = 'Server name'
    owner = 'Server ownership transfers'
    region = 'Voice server region'
    filter = 'Explicit content filter'
    verification = 'Verification level changes'
    emoji = 'Emoji additions, removals, or renames'
    2fa = 'Two-factor authentication requirement for administrators'
    [guild.events.afk]
      timeout = 'How long members can be AFK in voice'
      channel = 'The voice channel AFK members will be moved to'


[channel]
  __name__ = 'Channel'
  __description__ = 'Channel creation, deletion, and update logging'

  user_limit_changed = '''\N{PENCIL} User limit for channel {mention} (`{id}`) has been {after, plural,
        =0 {removed}
       one {changed to # member}
     other {changed to # members}
   }'''
  position_changed = '\N{PENCIL} Channel {mention} (`{id}`) moved from position **`#{before}`** to **`#{after}`**'
  category_changed = '\N{PENCIL} Channel {mention} (`{id}`) has been moved to category {category} (`{cat_id}`)'
  category_uncategorized = '\N{PENCIL} Channel {mention} (`{id}`) has been uncategorized'

  created = '\N{SCROLL} Channel {channel} (`{id}`) has been created'
  created_responsible = '\N{SCROLL} {moderator} (`{mod_id}`) created channel {channel} (`{id}`)'
  deleted = '\N{WASTEBASKET} Channel `{channel}` (`{id}`) has been deleted'
  deleted_responsible = '\N{WASTEBASKET} {moderator} (`{mod_id}`) deleted channel `{channel}` (`{id}`)'

  name_changed = '\N{PENCIL} Name for channel {mention} (`{id}`) has been changed from `{before}` to `{after}`'
  name_changed_responsible = '\N{PENCIL} {moderator} (`{mod_id}`) changed name for channel {mention} (`{id}`) from `{before}` to `{after}`'

  now_nsfw = '\N{CROSS MARK} Channel {mention} (`{id}`) is now marked as NSFW'
  now_nsfw_responsible = '\N{CROSS MARK} {moderator} (`{mod_id}`) marked channel {mention} (`{id}`) as NSFW'
  no_longer_nsfw = '\N{CROSS MARK} Channel {mention} (`{id}`) is no longer marked as NSFW'
  no_longer_nsfw_responsible = '\N{CROSS MARK} {moderator} (`{mod_id}`) unmarked channel {mention} (`{id}`) as NSFW'

  topic_changed = '\N{PENCIL} Topic for channel {mention} (`{id}`) has been updated'
  topic_changed_responsible = '\N{PENCIL} {moderator} (`{mod_id}`) updated topic for channel {mention} (`{id}`)'

  bitrate_changed = '\N{SPEAKER WITH ONE SOUND WAVE} Bitrate for channel {mention} (`{id}`) has been changed to {after}'
  bitrate_changed_responsible = '\N{SPEAKER WITH ONE SOUND WAVE} {moderator} (`{mod_id}`) changed bitrate for channel {mention} (`{id}`) to {after}'

  overwrites_changed = '\N{CLOSED LOCK WITH KEY} Overwrites for {type, select, member {member} role {role}} {item} (`{item_id}`) in channel {mention} (`{id}`) were updated'
  overwrites_changed_responsible = '\N{CLOSED LOCK WITH KEY} {moderator} (`{mod_id}`) updated overwrites for {type, select, member {member} role {role}} {item} (`{item_id}`) in channel {mention} (`{id}`)'

  slowmode_changed = '\N{STOPWATCH} Slow mode for channel {mention} (`{id}`) has been set to {slow, plural, one {# second} other {# seconds}}'
  slowmode_changed_responsible = '\N{STOPWATCH} {moderator} (`{mod_id}`) set slow mode for channel {mention} (`{id}`) to {slow, plural, one {# second} other {# seconds}}'

  [channel.events]
    create = 'Channel creations'
    delete = 'Channel deletions'
    [channel.events.update]
      name = 'Channel name changes'
      category = 'Channel category changes'
      position = 'Channel position changes'
      topic = 'Channel topic changes'
      bitrate = 'Channel bitrate changes'
      user_limit = 'Channel user limit changes'
      nsfw = 'Channel NSFW flag toggle'
      overwrites = 'Channel permission overwrites'
      slowmode = 'Slow mode duration changes'


[member]
  __name__ = 'Member'
  __description__ = 'Member joining, leaving, and update logging'

  join = '\N{INBOX TRAY} Member {member} (`{id}`) joined the server'
  leave = '\N{OUTBOX TRAY} Member {member} (`{id}`) left the server'

  name = '\N{LABEL} Member {member} (`{id}`) changed their name from `{before}`'

  roles = '\N{LOCK WITH INK PEN} Roles for member {member} (`{id}`) updated'
  roles_responsible = '\N{LOCK WITH INK PEN} {moderator} (`{mod_id}`) updated roles for member {member} (`{id}`)'
  roles_self = '\N{LOCK WITH INK PEN} **{moderator}** (`{mod_id}`) updated their own roles'

  nick = '\N{LABEL} Nickname for member {member} (`{id}`) was changed to `{nick}`'
  nick_responsible = '\N{LABEL} {moderator} (`{mod_id}`) changed nickname for member {member} (`{id}`) to `{nick}`'
  nick_self = '\N{LABEL} Member {member} (`{id}`) changed their nickname to `{nick}`'

  [member.events]
    join = 'Member joining'
    leave = 'Member leaving'
    [member.events.update]
      name = 'Member username changes'
      nick = 'Member nickname changes'
      roles = 'Member role changes'


[message]
  __name__ = 'Message'
  __description__ = 'Message edit and deletion logging'

  edited = '\N{PENCIL} Message `{mid}` sent by {author} (`{id}`) sent in {channel} was edited'
  sys_deleted = '\N{WASTEBASKET} A system message in channel {channel} was deleted'
  deleted = '\N{WASTEBASKET} Message `{mid}` sent by {author} (`{id}`) sent in {channel} was deleted'
  filtered = '\N{SPEAK-NO-EVIL MONKEY} Message `{mid}` sent by {author} (`{id}`) sent in {channel} was automatically filtered'
  deleted_bulk = '\N{WASTEBASKET} **{num}** messages were bulk deleted from {channel}'

  message_content = 'Message Content'
  attachments = 'Attachments'
  filtered_terms = 'Filtered terms hit'

  [message.events]
    edit = 'Message edits'
    delete = 'Message deletions (only logs cached messages)'
    bulk = 'Bulk message deletions'
    filtered = 'Filter message deletions'


[role]
  __name__ = 'Role'
  __description__ = 'Role creation, deletion and update logging'

  created = '\N{CROSSED SWORDS} Role {name} (`{id}`) has been created'
  created_responsible = '\N{CROSSED SWORDS} {moderator} (`{mod_id}`) created role {name} (`{id}`)'

  deleted = '\N{WASTEBASKET} Role `{name}` (`{id}`) has been deleted'
  deleted_responsible = '\N{WASTEBASKET} {moderator} (`{mod_id}`) deleted role `{name}` (`{id}`)'

  name_changed = '\N{PENCIL} Name for role {name} (`{id}`) has been changed from `{before}`'
  name_changed_responsible = '\N{PENCIL} {moderator} (`{mod_id}`) changed name for role {name} (`{id}`) from `{before}`'

  permissions_changed = '\N{CLOSED LOCK WITH KEY} Permissions for role {name} (`{id}`) have been updated'
  permissions_changed_responsible = '\N{CLOSED LOCK WITH KEY} {moderator} (`{mod_id}`) updated permissions for role {name} (`{id}`)'

  colour_changed = '\N{LOWER LEFT PAINTBRUSH} Role {name} (`{id}`) colour changed from `{before}` to `{after}`'
  colour_changed_responsible = '\N{LOWER LEFT PAINTBRUSH} {moderator} (`{mod_id}`) changed colour for role {name} (`{id}`) from `{before}` to `{after}`'

  now_mentionable = '\N{BELL} Role {name} (`{id}`) is now mentionable'
  not_mentionable = '\N{BELL WITH CANCELLATION STROKE} Role {name} (`{id}`) is no longer mentionable'
  now_mentionable_responsible = '\N{BELL} {moderator} (`{mod_id}`) made role role {name} (`{id}`) mentionable'
  not_mentionable_responsible = '\N{BELL} {moderator} (`{mod_id}`) made role role {name} (`{id}`) unmentionable'

  now_hoisted = '\N{PENCIL} Role {name} (`{id}`) is now hoisted'
  not_hoisted = '\N{PENCIL} Role {name} (`{id}`) is no longer hoisted'
  now_hoisted_responsible = '\N{PENCIL} {moderator} (`{mod_id}`) hoisted role {name} (`{id}`)'
  not_hoisted_responsible = '\N{PENCIL} {moderator} (`{mod_id}`) unhoisted role {name} (`{id}`)'

  position_changed = '\N{PENCIL} Role {name} (`{id}`) position changed from **`{before}`** to **`{after}`**'

  # These are used in the `create` event.
  hoisted = 'Displayed seperately'
  mentionable = 'Mentionable'
  colour = 'Colour'
  permissions = 'Permissions'

  [role.events]
    create = 'Role creations'
    delete = 'Role deletions'
    [role.events.update]
      name = 'Role name changes'
      permissions = 'Role permission changes'
      colour = 'Role colour changes'
      mentionable = 'Mentionable status toggle'
      hoist = 'Display seperately from online members'
      position = 'Role hiearchy position changes'


[voice]
  __name__ = 'Voice'
  __description__ = 'Voice status logging'

  mute = '\N{FACE WITHOUT MOUTH} `{member}` (`{id}`) is now {type, select, self {self} server {server}} muted'
  unmute = '\N{FACE WITH OPEN MOUTH} `{member}` (`{id}`) is no longer {type, select, self {self} server {server}} muted'
  deaf = '\N{SPEAKER WITH CANCELLATION STROKE} `{member}` (`{id}`) is now {type, select, self {self} server {server}} deafened'
  undeaf = '\N{SPEAKER} `{member}` (`{id}`) is no longer {type, select, self {self} server {server}} deafened'

  [voice.channel]
    join = '\N{MULTIPLE MUSICAL NOTES} `{member}` (`{id}`) joined voice channel {after} (`{aid}`)'
    leave = '\N{MULTIPLE MUSICAL NOTES} `{member}` (`{id}`) left voice channel {before} (`{bid}`)'
    switch = '\N{MULTIPLE MUSICAL NOTES} `{member}` (`{id}`) switched from voice channel {before} (`{bid}`) to {after} (`{aid}`)'

  [voice.events]
    channel = 'Joining and leaving voice channels'
    [voice.events.mute]
      self = 'Self mute'
      server = 'Server mute'
    [voice.events.deaf]
      self = 'Self deaf'
      server = 'Server deaf'
