import asyncio
from contextlib import suppress
from typing import List, Sequence

import discord
from discord.raw_models import RawBulkMessageDeleteEvent
from redbot.core import commands
from redbot.core.bot import Red

from overseer.modules import Bot, Channel, Guild, Member, Message, Role, Voice


# We do a large variety of very weird things here, which confuses pylint into raising
# these warnings, despite not being applicable.
# pylint:disable=too-many-function-args

# noinspection PyMethodMayBeStatic
class Events:
    bot: Red
    _messages_deleted: List[discord.Message]

    @commands.Cog.listener()
    async def on_filter_message_delete(self, message: discord.Message, hits: Sequence[str]):
        # filter dispatches this event after deleting the message, so we can fairly safely assume
        # that we can remove it
        self._messages_deleted.remove(message)
        await Message(message.guild).filtered(message, hits)

    @commands.Cog.listener()
    async def on_message_delete(self, message: discord.Message):
        self._messages_deleted.append(message)
        await asyncio.sleep(0.4)
        try:
            self._messages_deleted.remove(message)
        except ValueError:
            # it's either been filtered or bulk deleted, so we're just going to discard
            # this event so we don't either pollute logs with duplicate logs, or get ourselves
            # rate limited trying to log every deleted message from a bulk deletion
            return

        if not hasattr(message, "guild") or message.guild is None:
            return
        await Message(message.guild).delete(message)

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        if not after.guild:
            return
        await Message(after.guild).edit(before, after)

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload: RawBulkMessageDeleteEvent):
        for mid in payload.message_ids:
            with suppress(ValueError, StopIteration):
                self._messages_deleted.remove(
                    next(iter(x for x in self._messages_deleted if x.id == mid))
                )

        channel: discord.TextChannel = self.bot.get_channel(payload.channel_id)
        if not getattr(channel, "guild", None):
            return
        await Message(channel.guild).bulk_delete(channel, payload.message_ids)

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        await Member(member.guild).join(member)

    @commands.Cog.listener()
    async def on_member_leave(self, member: discord.Member):
        await Member(member.guild).leave(member)

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        await Member(after.guild).update(before, after)

    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel: discord.abc.GuildChannel):
        # noinspection PyUnresolvedReferences
        await Channel(channel.guild).create(channel)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel: discord.abc.GuildChannel):
        # noinspection PyUnresolvedReferences
        await Channel(channel.guild).delete(channel)

    @commands.Cog.listener()
    async def on_guild_channel_update(
        self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel
    ):
        # noinspection PyUnresolvedReferences
        await Channel(after.guild).update(before, after)

    @commands.Cog.listener()
    async def on_voice_state_update(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if not getattr(member, "guild", None):
            return
        await Voice(member.guild).update(member, before, after)

    @commands.Cog.listener()
    async def on_guild_role_create(self, role: discord.Role):
        await Role(role.guild).create(role)

    @commands.Cog.listener()
    async def on_guild_role_delete(self, role: discord.Role):
        await Role(role.guild).delete(role)

    @commands.Cog.listener()
    async def on_guild_role_update(self, before: discord.Role, after: discord.Role):
        await Role(after.guild).update(before, after)

    @commands.Cog.listener()
    async def on_guild_update(self, before: discord.Guild, after: discord.Guild):
        await Guild(after).update(before, after)

    @commands.Cog.listener()
    async def on_guild_emojis_update(
        self, guild: discord.Guild, before: List[discord.Emoji], after: List[discord.Emoji]
    ):
        await Guild(guild).emoji(before, after)

    @commands.Cog.listener()
    async def on_command_error(self, ctx: commands.Context, error: Exception):
        # ignore common user error exceptions
        if isinstance(
            error,
            (
                commands.UserInputError,
                commands.CheckFailure,
                commands.CommandOnCooldown,
                commands.DisabledCommand,
                commands.CommandNotFound,
            ),
        ):
            return

        if isinstance(error, commands.CommandInvokeError):
            error = error.original

        await Bot().command_error(ctx, error)
