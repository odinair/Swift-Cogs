import traceback
from collections import OrderedDict
from copy import deepcopy
from datetime import timedelta, datetime
from textwrap import indent
from typing import Dict, List, AsyncIterable, Union, Tuple

import discord
from redbot.core import checks, commands
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import bold, pagify, warning, info
from redbot.vendored.discord.ext import menus
from redbot.vendored.discord.ext.menus import MenuPages, ListPageSource as _ListPageSource

from overseer.converters import SettingsConverter, ModuleConverter, status_diff
from overseer.core import Module, config, translate, Logger, version_info
from overseer.utils import tick, flatten, deep_merge

# noinspection PyProtectedMember
from overseer.core.module import modules, teardown
from overseer.events import Events
from overseer.modules import Bot
from swift_i18n import Humanize


class ConfirmMenu(menus.Menu):
    def __init__(self, content: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.content = content
        self.result = None

    async def send_initial_message(self, ctx, channel):
        return await channel.send(self.content)

    @menus.button("\N{WHITE HEAVY CHECK MARK}")
    async def do_confirm(self, _):
        self.result = True
        self.stop()

    @menus.button("\N{CROSS MARK}")
    async def do_deny(self, _):
        self.result = False
        self.stop()

    async def prompt(self, ctx):
        await self.start(ctx, wait=True)
        return self.result


class ListPageSource(_ListPageSource):
    def format_page(self, menu, page):
        return page


async def pagify_modules(ctx: commands.Context) -> AsyncIterable[discord.Embed]:
    """Build module setting embeds

    This is designed to somewhat mimic Red's built-in [p]help embeds.
    """
    # this is set relatively low since we're appending a lot of extra info that
    # we aren't counting towards our calculated total length
    max_len = 600
    colour = await ctx.embed_colour()
    mods: List[Module] = list(
        filter(lambda x: x is not None, [await mod.from_ctx(ctx) for mod in modules.values()])
    )
    if not mods:
        return

    all_settings: Dict[Module, Dict[Logger, Union[bool, int]]] = {}
    channels = {}
    for mod in mods:
        mod_settings = flatten(deep_merge(await mod.conf.all(), deepcopy(mod.conf.defaults)))
        all_settings[mod] = {
            logger: mod_settings.get(logger.key, False)
            for logger in mod.loggers
            if await logger.passes_checks(mod)
        }
        channels[mod] = ctx.bot.get_channel(mod_settings.get("_log_channel"))

    def _format_opt(module: Module, logger: Logger) -> str:
        status = all_settings[module].get(logger, False)
        if channels[mod] is None and isinstance(status, bool):
            status = False

        # TIL isinstance(True, int) is True
        if not isinstance(status, bool):
            channel = ctx.bot.get_channel(status)
            if channel:
                return translate(
                    "module_status.override_channel",
                    module=module,
                    logger=logger,
                    channel=channel.mention,
                    description=logger.description,
                )
        return translate(
            "module_status",
            "enabled" if status is True else "disabled",
            module=module,
            logger=logger,
            description=logger.description,
        )

    # Modules in this list will have `(continued)` appended to their name,
    # and the lengthy module description won't be prepended to the list of settings
    continuing_modules: List[Module] = []
    curr_len = 0
    curr_page: Dict[Module, List[str]] = OrderedDict()
    # noinspection PyUnresolvedReferences
    base_embed = discord.Embed(
        colour=colour, description=translate("usage_info", prefix=ctx.clean_prefix)
    ).set_author(
        name=translate("overseer_settings"), icon_url=str(ctx.me.avatar_url_as(format="png"))
    )

    async def _yield():
        nonlocal curr_page
        nonlocal curr_len

        embed = base_embed.copy()
        for m, v in curr_page.items():
            name = bold(str(m.name))
            if m in continuing_modules:
                name = translate("continued", module=name)

            if m not in continuing_modules:
                channel = await m.log_destination()
                v = "{}\n{}\n\n{}".format(
                    translate(
                        "mod_desc",
                        description=m.description
                        if m.description
                        else translate("no_mod_description"),
                    ),
                    translate(
                        "logging_to",
                        "channel" if channel is not None else "nowhere",
                        dest=getattr(channel, "mention", None),
                        prefix=ctx.clean_prefix,
                        module=m,
                    ),
                    "\n".join(v),
                )
            else:
                v = "\n".join(v)
            embed.add_field(name=name, value=v, inline=False)

        continuing_modules.append(mod)
        curr_page = OrderedDict()
        curr_len = 0
        return embed

    for mod, loggers in all_settings.items():
        for logger in loggers.keys():
            if mod not in curr_page:
                curr_page[mod] = []
            val = _format_opt(mod, logger)
            curr_len += len(val)
            curr_page[mod].append(val)
            if curr_len >= max_len:
                yield await _yield()
        if curr_len >= max_len:
            yield await _yield()

    if curr_page:
        yield await _yield()


# noinspection PyMethodMayBeStatic
@translate.cog("help.cog_class")
class Overseer(commands.Cog, Events):
    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self._messages_deleted = []

    def cog_unload(self):
        teardown()

    async def red_delete_data_for_user(self, **_):
        pass  # nothing to delete

    async def post_load(self):
        if hasattr(self.bot, "wait_until_red_ready"):
            await self.bot.wait_until_red_ready()
        else:
            await self.bot.wait_until_ready()

        # only log the startup event if the bot has been started within the last 30 seconds
        # this should give us enough time to load and log the startup event, and
        # (re)loading the cog after this window won't log the event.
        if self.bot.uptime + timedelta(seconds=30) > datetime.utcnow():
            await Bot().startup()

    @commands.group(invoke_without_command=True, usage="[events...]")
    @commands.guild_only()
    @checks.guildowner_or_permissions(administrator=True)
    @checks.bot_has_permissions(add_reactions=True)
    @translate.command("help.root")
    async def overseer(self, ctx: commands.Context, *settings: SettingsConverter):
        if settings:
            to_update: Dict[str, List[Tuple[Logger, Union[bool, discord.TextChannel, None]]]] = {}
            for k, v in settings:
                if k not in to_update:
                    to_update[k] = []
                to_update[k].append(v)
            mods: Dict[str, Module] = {}
            updated: Dict[str, Dict[str, Union[bool, int]]] = {}

            for mod, lgrs in to_update.items():
                if mod not in mods:
                    mods[mod] = await modules.get(mod).from_ctx(ctx)
                mod = mods[mod]

                before = await mod.conf.all()
                await mod.set_events(*lgrs)
                after = await mod.conf.all()
                updated[mod.id] = flatten(status_diff(before, after))

            if to_update and not any(updated.values()):
                await ctx.send(translate("no_settings_changed"))
                return

            if any(updated.values()):
                summary = [bold(translate("settings_updated")), ""]
                for mod, opts in updated.items():
                    mod = modules.get(mod).name
                    mod_summary = []
                    for opt, new_val in opts.items():
                        if type(new_val).__name__ == "int":
                            trans_key = "setting_enabled_override"
                        else:
                            trans_key = "setting_{}".format(
                                "enabled" if new_val is True else "disabled"
                            )
                        mod_summary.append(
                            translate(
                                trans_key,
                                module=mod,
                                setting=opt,
                                channel=getattr(self.bot.get_channel(new_val), "mention", None),
                            )
                        )
                    summary.append(
                        "\n".join([f"\N{GEAR} **{mod}**", indent("\n".join(mod_summary), " " * 6)])
                    )
                for page in pagify(info("\n".join(summary))):
                    await ctx.send(page)
                return

        pages = [x async for x in pagify_modules(ctx)]
        if not pages:
            await ctx.send_help()
            return

        total = len(pages)
        for page in pages:
            index = pages.index(page)
            page.set_footer(
                text=" | ".join(
                    [
                        translate("page", current=index + 1, total=total),
                        translate("version", version=version_info),
                    ]
                )
            )
            pages[index] = page

        await MenuPages(ListPageSource(pages, per_page=1)).start(ctx)

    # This command should really be redone to not just dump a giant wall of text
    # on the user.
    @overseer.command(name="explain", aliases=["help"], hidden=True)
    @translate.command("help.explain")
    async def overseer_explain(self, ctx: commands.Context):
        for page in pagify(translate("overseer_explanation", prefix=ctx.clean_prefix)):
            await ctx.send(page)

    @overseer.command(
        name="destination", aliases=["dest", "channel"], usage="<module...> [channel]"
    )
    @translate.command("help.channel")
    async def overseer_channel(
        self,
        ctx: commands.Context,
        mods: commands.Greedy[ModuleConverter],
        channel: discord.TextChannel = None,
    ):
        if not mods:
            raise commands.BadArgument

        if channel and not channel.permissions_for(ctx.guild.me).send_messages:
            await ctx.send(warning(translate("channel_no_permissions")))
            return

        for module in mods:
            await module.conf.set_raw("_log_channel", value=getattr(channel, "id", None))
        if channel:
            await ctx.send(
                tick(
                    translate(
                        "set_log_channel",
                        mods=Humanize([bold(x.name) for x in mods]),
                        n=len(mods),
                        channel=channel.mention,
                    )
                )
            )
        else:
            await ctx.send(
                tick(
                    translate(
                        "cleared_log_channel",
                        mods=Humanize([bold(x.name) for x in mods]),
                        n=len(mods),
                    )
                )
            )

    @overseer.command(name="reset")
    @translate.command("help.reset")
    async def overseer_reset(self, ctx: commands.Context, *, module: ModuleConverter = None):
        if module is not None:
            # noinspection PyUnresolvedReferences
            cfg = module.conf
        else:
            cfg = config.guild(ctx.guild)

        if await ConfirmMenu(
            warning(
                translate(
                    "reset_confirmation" if module else "reset_server_confirmation",
                    module=getattr(module, "name", translate("unknown")),
                )
            )
        ).prompt(ctx):
            await cfg.clear()
            await ctx.send(tick(translate("reset_success")))
        else:
            await ctx.send(translate("ok_then"))

    @overseer.command(name="audit")
    @translate.command("help.audit")
    async def overseer_audit(self, ctx: commands.Context, toggle: bool = None):
        if toggle is None:
            toggle = not await config.guild(ctx.guild).get_raw("_responsible_mod", default=False)
        await config.guild(ctx.guild).set_raw("_responsible_mod", value=toggle)
        await ctx.send(tick(translate("audit_enabled" if toggle else "audit_disabled")))

    @overseer.command(name="debug_log", hidden=True)
    @checks.is_owner()
    async def overseer_debug(self, ctx: commands.Context):
        await Bot().startup()
        await ctx.tick()

    @overseer.command(name="reloadlocales", hidden=True)
    @checks.is_owner()
    async def overseer_reload(self, ctx: commands.Context):
        translate.locales.dump()
        for locale in translate.locales:
            # noinspection PyBroadException
            try:
                getattr(locale, "strs")
            except Exception:  # pylint:disable=broad-except
                exc = traceback.format_exc()
                await ctx.send_interactive(
                    pagify(f"# Failed to reload locale {locale!s}\n\n{exc}"),
                    box_lang="py",
                    timeout=30,
                )
        await ctx.tick()
