try:
    from swift_i18n import red
except ImportError:
    from redbot.core.errors import CogLoadError

    raise CogLoadError(
        "Your bot has an outdated version of a library this cog depends on installed; please refer"
        " to this document to resolve this: <https://u.odinair.xyz/update_swift_i18n.md>"
    )

from pathlib import Path
import json

with open(str(Path(__file__).parent / "info.json")) as f:
    __red_end_user_data_statement__ = json.load(f)["end_user_data_statement"]


def setup(bot):
    from .colourrole import ColourRole

    bot.add_cog(ColourRole(bot))
