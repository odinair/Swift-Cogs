from discord.ext.commands import CommandError


class StarboardException(CommandError):
    """Root Starboard cog exception"""


class StarException(StarboardException):
    """Failed to add or remove a star"""


class BlockedException(StarboardException):
    """Generic starboard ignore exception"""


class BlockedUserException(BlockedException):
    """User attempting to add a star is blocked from the starboard"""


class BlockedAuthorException(BlockedException):
    """Message author is blocked from the starboard"""


class IgnoredChannelException(BlockedException):
    """Channel a message is located in is blocked from the starboard"""


class SelfStarException(BlockedException):
    """Self-star attempt while the guild disallows self-starring"""
