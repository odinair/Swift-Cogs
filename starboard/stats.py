from collections import Counter
from typing import Dict, Tuple, Union

import discord

from starboard.db import Database
from starboard.message import resolve_starred_by
from starboard.shared import log
from starboard.worker import Worker, WorkerGuild

__all__ = ("gen_statistics",)


def sort_dict(d: dict):
    return {x: y for x, y in reversed(sorted(d.items(), key=lambda x: x[1])) if y}


async def gen_statistics(
    guild: discord.Guild, *filter_to: Union[discord.Member, int], top: int = 0
) -> Dict[str, Union[Dict[Union[discord.Member, int], int], int]]:
    starboard: WorkerGuild = Worker.get_guild(guild)
    filter_to = {x.id if isinstance(x, discord.Member) else x for x in filter_to}
    data: dict = {
        "given": Counter(),
        "received": Counter(),
        "max_received": {},
        "sb_messages": Counter(),
        "total_given": 0,
        "total_messages": 0,
    }

    # noinspection PyShadowingNames
    def m(member_id: int) -> Tuple[Union[discord.Member, int], bool]:
        mb = guild.get_member(member_id)
        if not mb:
            return (member_id, not filter_to or member_id in filter_to)
        else:
            return (mb, not mb.bot and (not filter_to or mb.id in filter_to))

    log.debug("Building starboard statistics for guild %r", guild)

    async for _, mdata in Database.all_messages(guild):
        if mdata.get("hidden"):
            continue

        author_id = mdata.get("author_id")
        if author_id is None:
            continue
        author, allow = m(author_id)
        if getattr(author, "bot", False):
            continue

        starrers = resolve_starred_by(mdata)
        if len(starrers):
            data["total_messages"] += 1
            data["total_given"] += len(starrers)

        for give_id in starrers:
            if give_id == 0xDE3:  # deleted user
                continue
            giver, giver_allow = m(give_id)
            if giver and giver_allow and (giver != author or starboard.self_star):
                data["given"][giver] += 1

        if not allow:
            continue

        received = len([x for x in starrers if x != author_id or starboard.self_star])
        data["received"][author] += received
        data["max_received"][author] = max(received, data["max_received"].get(author, 0))

        if mdata.get("starboard_message"):
            data["sb_messages"][author] += 1

    data = {k: sort_dict(v) if isinstance(v, dict) else v for k, v in data.items()}
    data = (
        {k: dict(list(v.items())[:top]) if isinstance(v, dict) else v for k, v in data.items()}
        if top > 0
        else data
    )

    return data
