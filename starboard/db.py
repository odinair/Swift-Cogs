from typing import AsyncIterable, Optional, Tuple, Union

import discord

# noinspection PyProtectedMember
from redbot.core.config import Group
from redbot.core.utils import AsyncIter


# TODO this is largely a remnant of when starboard used to store messages separately from
#   config data when config was configured to use Mongo, and as such could probably be
#   refactored away at some point
class Database:
    @staticmethod
    def _config(*ids) -> Group:
        from .shared import config

        return config.custom("MESSAGES", *ids)

    @classmethod
    async def all_guilds(cls) -> AsyncIterable[Union[discord.Guild, discord.Object]]:
        """Get *all* guilds with stored data

        This should only be used when you absolutely need to (such as for data
        retrieval/deletion requests). In most cases, iterating through
        ``bot.guilds`` should be enough.

        Yielded guilds may either be an actual :class:`discord.Guild` or an
        :class:`discord.Object` with only an attached ID.
        """
        from .shared import bot, config

        gids = set((await config.all_guilds()).keys())
        async for gid in AsyncIter(gids, steps=100):
            if guild := bot.get_guild(gid):
                yield guild
            else:
                yield discord.Object(id=gid)

    @classmethod
    async def remove_message(cls, guild: discord.Guild, mid: int) -> None:
        from .shared import log

        log.debug("Deleting message %s in guild %r from Config", mid, guild)
        await cls._config(guild.id, mid).clear()

    @classmethod
    async def get_message(cls, guild: discord.Guild, mid: int) -> Optional[dict]:
        return await cls._config(guild.id, mid)(default={}) or None

    @classmethod
    async def save_message(cls, guild: discord.Guild, mid: int, data: dict):
        if "id" in data:
            del data["id"]
        await cls._config(guild.id, mid).set(data)

    @classmethod
    async def all_messages(cls, guild: discord.Guild) -> AsyncIterable[Tuple[int, dict]]:
        async for mid, data in AsyncIter((await cls._config(guild.id).all()).items(), steps=2_500):
            yield mid, data

    @classmethod
    async def find(cls, guild: discord.Guild, message_filter: dict) -> Optional[Tuple[int, dict]]:
        # Config with large data sets is *infinite* amounts of joys to work with.
        async for mid, message in cls.all_messages(guild):
            if all(k in message and message.get(k) == v for k, v in message_filter.items()):
                return mid, message
