from abc import ABC

from redbot.core import commands
from redbot.core.bot import Red

from starboard.worker import WorkerGuild


class Context(commands.Context):
    starboard: WorkerGuild


class MixinMeta:
    bot: Red
    qualified_name: str


class CompositeMetaClass(type(commands.Cog), type(ABC)):
    pass
