from typing import Literal, MutableMapping, Optional

import discord
from redbot.core import commands
from redbot.core.utils.caching import LRUDict

from starboard.core.abc import CompositeMetaClass, Context, MixinMeta
from starboard.db import Database
from starboard.exceptions import BlockedException, StarboardException
from starboard.message import StarboardMessage, resolve_starred_by
from starboard.shared import log
from starboard.worker import Worker, WorkerGuild

mcache: MutableMapping[int, Optional[int]] = LRUDict(size=10_000)  # noqa


# noinspection PyMethodMayBeStatic
class Events(MixinMeta, metaclass=CompositeMetaClass):
    def cog_check(self, ctx: Context):
        if not ctx.guild:
            raise commands.NoPrivateMessage()
        return True

    async def cog_before_invoke(self, ctx: Context):
        # we can pretty safely assume that we're in a guild context, so let's
        # inject some commonly retrieved data to the context object we're given
        guild = Worker.get_guild(ctx.guild)
        await guild.load()
        ctx.starboard = guild

    def cog_unload(self):
        Worker.teardown()

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        # get_guild has handling for guilds not having a worker, and as such we can simply
        # use this to ignore the event if this is an erroneous event, or add the guild to an
        # available worker if this is actually a new guild
        Worker.get_guild(guild)

    @commands.Cog.listener()
    async def on_guild_remove(self, guild: discord.Guild):
        Worker.remove_guild(guild)

    @commands.Cog.listener()
    async def on_raw_message_edit(self, payload: discord.RawMessageUpdateEvent):
        channel = self.bot.get_channel(payload.data["channel_id"])
        if isinstance(channel, (discord.abc.PrivateChannel, type(None))):
            return
        guild = channel.guild
        if await self.bot.cog_disabled_in_guild_raw(self.qualified_name, guild.id):
            return
        starboard: WorkerGuild = Worker.get_guild(guild)
        message = await starboard.get_message(message_id=payload.message_id, cache_only=True)
        if message is not None:
            await message.update_cached_message()
            message.update()

    async def _get_message(self, payload: discord.RawReactionActionEvent) -> dict:
        if await self.bot.cog_disabled_in_guild_raw(self.qualified_name, payload.guild_id):
            return {}
        if not self.bot.get_guild(payload.guild_id):
            return {}

        starboard = Worker.get_guild(self.bot.get_guild(payload.guild_id))
        await starboard.load()
        if starboard.channel is None:
            return {}
        emoji: discord.PartialEmoji = payload.emoji
        if str(starboard.emoji) != str(emoji):
            return {}

        channel: discord.TextChannel = self.bot.get_channel(payload.channel_id)
        if (
            channel is None
            or isinstance(channel, discord.abc.PrivateChannel)
            or not getattr(channel, "guild", None)
        ):
            return {}

        guild: discord.Guild = channel.guild
        member: discord.Member = guild.get_member(payload.user_id)
        if channel == starboard.channel:
            if payload.message_id not in mcache:
                m = await Database.find(guild, {"starboard_message": payload.message_id})
                if not m:
                    return {}
                mcache[payload.message_id] = int(m[0])
            message = mcache[payload.message_id]
        else:
            message = payload.message_id

        return {
            "message": await starboard.get_message(message_id=message, channel=channel),
            "member": member,
            "channel": channel,
            "emoji": emoji,
        }

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        data = await self._get_message(payload)
        message: StarboardMessage = data.get("message")
        member: discord.Member = data.get("member")
        emoji: discord.PartialEmoji = data.get("emoji")
        channel: discord.TextChannel = data.get("channel")
        if not message:
            return

        try:
            message.add_star(member)
        except BlockedException:
            if channel.permissions_for(channel.guild.me).manage_messages:
                try:
                    await message.message.remove_reaction(emoji=emoji, member=member)
                except discord.HTTPException:
                    pass
        except StarboardException:
            pass

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        data = await self._get_message(payload)
        message: StarboardMessage = data.get("message")
        member: discord.Member = data.get("member")
        if not message:
            return

        try:
            message.remove_star(member)
        except StarboardException:
            pass

    @commands.Cog.listener()
    async def on_raw_reaction_clear(self, payload: discord.RawReactionClearEvent):
        if await self.bot.cog_disabled_in_guild_raw(self.qualified_name, payload.guild_id):
            return {}
        channel: discord.TextChannel = self.bot.get_channel(payload.channel_id)
        if channel is None or isinstance(channel, discord.abc.PrivateChannel):
            return
        starboard: WorkerGuild = Worker.get_guild(channel.guild)
        await starboard.load()
        message = await starboard.get_message(message_id=payload.message_id)
        if message is None:
            return
        message.starrers = []
        message.update()

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        if await self.bot.cog_disabled_in_guild_raw(self.qualified_name, payload.guild_id):
            return {}
        guild = self.bot.get_guild(payload.guild_id)
        if not guild:
            return

        sb: WorkerGuild = Worker.get_guild(guild)
        await sb.load()
        if not sb.channel:
            return

        mdata = await Database.get_message(guild, payload.message_id)
        if mdata is None:
            return

        if mdata.get("starboard_message"):
            try:
                await self.bot.http.delete_message(sb.channel.id, mdata["starboard_message"])
            except discord.NotFound:
                pass
            except discord.HTTPException:
                log.exception("Failed to delete starboard message for a now deleted source message")

        await Database.remove_message(guild, payload.message_id)

    async def red_delete_data_for_user(
        self,
        *,
        requester: Literal["discord_deleted_user", "owner", "user", "user_strict"],
        user_id: int,
    ):
        if requester not in ("discord_deleted_user", "owner", "user", "user_strict"):
            log.warning(
                "Unrecognized data deletion requester %r; ignoring request for user ID %r",
                requester,
                user_id,
            )
            return

        # restrict to the bot owner and Discord to prevent users from either flooding starboard
        # channels with the same message and/or adding junk stars. if users want to remove
        # their own messages from the starboard, they can do so by deleting the source
        # message, even if it isn't in the message cache.
        if requester not in ("discord_deleted_user", "owner"):
            return

        async for guild in Database.all_guilds():
            worker = Worker.get_guild(guild) if isinstance(guild, discord.Guild) else None
            async for mid, data in Database.all_messages(guild):
                if data["author_id"] == user_id:
                    if worker:
                        worker.remove(mid)
                    await Database.remove_message(guild, mid)
                elif user_id in (starrers := resolve_starred_by(data)):
                    starrers.remove(user_id)
                    starrers.append(0xDE3)
                    await Database.save_message(guild, mid, data)
                    if worker:
                        worker.remove(mid)
