import json

from redbot.core import checks, commands
from redbot.core.commands import GuildConverter
from redbot.core.utils.chat_formatting import pagify

from starboard.core.abc import CompositeMetaClass, Context, MixinMeta
from starboard.db import Database
from starboard.shared import _, config, tick
from starboard.worker import Worker


class StarboardSetCommand(MixinMeta, metaclass=CompositeMetaClass):
    @commands.group(name="starboardset")
    @checks.bot_in_a_guild()
    @checks.is_owner()
    async def starboardset(self, ctx: Context):
        """Global Starboard management"""

    @starboardset.command(name="json", hidden=True)
    async def starboardset_json(self, ctx: Context, message_id: int, guild_id: int = None):
        """Dump the data for a message as raw JSON"""
        if guild_id is None and ctx.guild is None:
            await ctx.send(_("You must specify a guild ID in a DM context.").format())
            return

        guild = ctx.guild if guild_id is None else self.bot.get_guild(guild_id)
        if guild is None:
            await ctx.send(
                _("The guild ID you provided does not refer to a guild I'm in.").format()
            )
            return

        data = await Database.get_message(guild, message_id)
        if data is None:
            return await ctx.send(_("No data for message `{id}` exists.").format(id=message_id))
        await ctx.send_interactive(pagify(json.dumps(data, indent=2)), box_lang="json")

    @starboardset.command(name="restartworker")
    async def starboardset_restartworker(self, ctx: Context, guild: GuildConverter = None):
        """Forcibly restart the worker for the current or given guild"""
        worker = ctx.starboard if guild is None else Worker.get_worker(guild)
        if not worker:
            Worker.add_guild(guild)
        else:
            worker.restart()
        await ctx.tick()

    @starboardset.command(name="perworker", aliases=["per"])
    async def starboardset_perworker(self, ctx: commands.Context, guilds: int):
        """Set how many servers should be provisioned per worker

        This defaults to 10 servers; you shouldn't need to modify this on small bots.
        """
        if guilds < 0:
            await ctx.send(_("Workers must have at least one guild").format())
            return
        await config.per_worker.set(guilds)
        Worker.per_worker = guilds
        await ctx.send(
            tick(
                _(
                    "Workers will now service {count, plural, one {1 server}"
                    " other {{count} servers}} each. This will only fully take effect"
                    " once the cog is reloaded."
                ).format(count=guilds)
            )
        )
