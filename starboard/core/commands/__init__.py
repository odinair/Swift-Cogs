from starboard.core.abc import CompositeMetaClass
from starboard.core.commands.star import StarCommand
from starboard.core.commands.starboard import StarboardCommand
from starboard.core.commands.starboardset import StarboardSetCommand


class Commands(StarboardCommand, StarCommand, StarboardSetCommand, metaclass=CompositeMetaClass):
    pass
