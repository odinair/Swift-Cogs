from __future__ import annotations

import asyncio
from datetime import datetime, timedelta
from random import randint
from typing import (
    ClassVar,
    Dict,
    Iterable,
    List,
    MutableMapping,
    NoReturn,
    Optional,
    Sequence,
    Tuple,
    Union,
)

import discord
from discord.ext.tasks import loop

from starboard import shared
from starboard.db import Database
from starboard.message import StarboardMessage
from starboard.shared import log

__all__ = ("Worker", "WorkerGuild", "DEFAULT_EMOJI")
DEFAULT_EMOJI = "\N{WHITE MEDIUM STAR}"


def chunks(seq: Sequence, chunk_every: int) -> Iterable[Sequence]:
    for i in range(0, len(seq), chunk_every):
        yield seq[i : i + chunk_every]


class ExtendedQueue(asyncio.Queue):
    def __contains__(self, item):
        return item in getattr(self, "_queue")

    def __len__(self):
        return len(getattr(self, "_queue"))

    async def __aiter__(self):
        while True:
            yield await self.get()


class Worker:
    __slots__ = ("guilds", "tasks", "queue")

    workers: ClassVar[List[Worker]] = []
    per_worker: ClassVar[int] = ...
    worker_tasks: ClassVar[Tuple[str, ...]] = ("messages", "cache")

    def __init__(self, guilds: List[discord.Guild]):
        self.guilds: Dict[discord.Guild, WorkerGuild] = {
            guild: WorkerGuild(self, guild) for guild in guilds
        }
        self.tasks: Dict[str, asyncio.Task] = {}
        self.queue = ExtendedQueue()

    def __repr__(self):
        return f"<Worker id={self.id} guild_count={len(self.guilds)} queue={len(self.queue)}>"

    def __contains__(self, item: discord.Guild):
        return item in self.guilds

    # region Global worker management methods

    @classmethod
    def get_worker(cls, guild: discord.Guild) -> Optional[Worker]:
        return next((x for x in cls.workers if guild in x), None)

    @classmethod
    def get_guild(cls, guild: discord.Guild) -> Optional[WorkerGuild]:
        worker = cls.get_worker(guild)
        if not worker:
            worker = cls.add_guild(guild)
        return worker.guilds[guild]

    @classmethod
    def add_guild(cls, guild: discord.Guild) -> Worker:
        if cls.per_worker is ...:
            raise RuntimeError("per_worker class variable is not set")
        next_available: Optional[Worker] = next(
            (x for x in cls.workers if len(x.guilds) < cls.per_worker), None
        )
        if next_available is None:
            log.debug("Creating worker #%s for guild %r", len(cls.workers) + 1, guild)
            next_available = cls([guild])
            cls.workers.append(next_available)
            next_available.poke()
        else:
            log.debug("Adding guild %r to worker %r", guild, next_available)
            next_available.guilds[guild] = WorkerGuild(next_available, guild)
        return next_available

    @classmethod
    def remove_guild(cls, guild: discord.Guild) -> None:
        worker = cls.get_worker(guild)
        if not worker:
            return
        del worker.guilds[guild]
        if not worker.guilds:
            worker.stop()
            log.debug("Removing worker #%s, as it now has no guilds to work for", worker.id)
            cls.workers.remove(worker)

    @classmethod
    async def bootstrap(cls):
        cls.per_worker = await shared.config.per_worker()
        if cls.per_worker < 1:
            default = shared.config.per_worker.default
            log.warning(
                "Guilds per worker configuration value is not a non-zero value;"
                " resetting to default value (%s).",
                default,
            )
            await shared.config.per_worker.clear()
            cls.per_worker = default
        await shared.bot.wait_until_ready()

        cls.workers = [cls(list(chunk)) for chunk in chunks(shared.bot.guilds, cls.per_worker)]
        log.debug("Spawned %s workers with up to %s guilds each", len(cls.workers), cls.per_worker)
        cls.supervisor.start()

    @classmethod
    def teardown(cls):
        cls.supervisor.cancel()
        cls.per_worker = ...
        for worker in cls.workers:
            worker.stop()
        cls.workers = []

    @staticmethod
    @loop(minutes=5)
    async def supervisor() -> None:
        workers = Worker.workers.copy()
        for worker in workers:
            if not worker.guilds:
                log.debug(
                    "Worker %s has no guilds to manage, pruning worker",
                    workers.index(worker) + 1,
                )
                worker.stop()
                Worker.workers.remove(worker)
                continue
            worker.poke()

    # endregion

    @property
    def id(self) -> int:
        return self.workers.index(self) + 1

    def status(self) -> Dict[str, bool]:
        """Check task statuses on the current Worker"""
        tasks = {name: not task.done() for name, task in self.tasks.items()}
        tasks.update({name: False for name in self.worker_tasks if name not in self.tasks})
        return tasks

    def start(self) -> None:
        """Start tasks on the current Worker"""
        self.log_exceptions()
        missing = [x for x in self.worker_tasks if x not in self.tasks]
        log.debug("Starting %s tasks for worker #%s: %s", len(missing), self.id, ", ".join(missing))
        self.tasks.update(
            {k: shared.bot.loop.create_task(getattr(self, f"_{k}_task")()) for k in missing}
        )

    def stop(self) -> None:
        """Stop tasks for the current Worker"""
        log.debug("Stopping all tasks for worker #%s", self.id)
        for name, task in self.tasks.items():
            if task and not task.done():
                log.debug("Stopping task %r on worker #%s", name, self.id)
                task.cancel()
        self.tasks = {}

    def restart(self) -> None:
        """Stop and restart all tasks on the current Worker"""
        self.stop()
        self.start()

    def poke(self) -> None:
        """Ensure the current Worker's tasks are still alive"""
        if not self.tasks:
            return self.start()

        if any(x.done() for x in self.tasks.values()):
            self.log_exceptions()
            self.start()

    def log_exceptions(self) -> None:
        """Log any errors from stopped Worker tasks

        Subsequent calls to this method will not do anything unless another error has occurred.
        """
        for task, error in self._errors():
            log.exception(
                "Task %r on worker #%s encountered an unexpected error",
                self.id,
                task,
                exc_info=error,
            )

    def _errors(self) -> List[Tuple[str, BaseException]]:
        excs: List[Tuple[str, BaseException]] = []
        ignored = (asyncio.CancelledError, asyncio.InvalidStateError)
        for name, task in list(self.tasks.items()):
            if task.done():
                try:
                    exc = task.exception()
                except ignored:
                    pass
                else:
                    if not isinstance(exc, ignored):
                        excs.append((name, exc))
                del self.tasks[name]
        return excs

    def clear(self):
        """Empty the update queue for the current Worker"""
        self.queue = ExtendedQueue()
        for guild in self.guilds.values():
            guild.flush()

    async def _messages_task(self) -> NoReturn:
        # Using `async for` on the message queue allows us to avoid sleeping for a few seconds
        # at a time and waking up constantly just to iterate through the queue, freeing up the
        # bot a bit to do more things, while also giving quicker feedback on stars.
        # This does have the downside of potentially causing us to hit rate limits quicker,
        # but that's a relatively acceptable trade-off in my opinion.
        async for message in self.queue:
            if message.guild not in self.guilds:
                continue
            await message.edit_message()
            await asyncio.sleep(1.3)

    # Technically this is NoReturn in most cases, but given the one return statement, it'd be
    # more accurate to label this as None.
    async def _cache_task(self) -> None:
        while True:
            if self not in self.workers:
                return self.stop()
            for guild in self.guilds.values():
                guild.prune()
            await asyncio.sleep(randint(60, 90))


class WorkerGuild:
    __slots__ = ("worker", "guild", "_config", "_cache")

    def __init__(self, worker: Worker, guild: discord.Guild):
        self.worker = worker
        self.guild = guild
        self._config: Optional[dict] = None
        # noinspection PyTypeChecker
        self._cache: MutableMapping[int, StarboardMessage] = {}

    @property
    def loaded(self) -> bool:
        """bool: Whether or not the current guild's data is loaded from Config"""
        return self._cache is not None

    @property
    def cache(self) -> List[StarboardMessage]:
        """List[StarboardMessage]: All currently cached starboard message objects"""
        return list(self._cache.values())

    def flush(self) -> None:
        """Empty the starboard message cache for this guild"""
        self._cache = {}

    def remove(self, message: Union[discord.Message, StarboardMessage, int]) -> None:
        """Remove a given message from the cache

        This does nothing if the given message is not in the cache.
        """
        message = getattr(message, "id", message)
        if message in self._cache:
            del self._cache[message]

    def prune(self, ttl=timedelta(minutes=30)) -> None:
        """Prune the cache of messages that haven't had activity in a while"""
        cutoff = datetime.utcnow() + ttl
        for item in list(self._cache.values()):
            if item.last_update > cutoff:
                log.debug("Pruning message %s from the cache", item.message.id)
                del self._cache[item.message.id]

    async def get_message(
        self,
        *,
        message: discord.Message = None,
        message_id: int = None,
        channel: discord.TextChannel = None,
        cache_only: bool = False,
    ) -> Optional[StarboardMessage]:
        """Get a starboard message

        Keyword Arguments
        ------------------
        message: discord.Message
            A message to retrieve a starboard message object for; if this isn't specified,
            ``message_id`` must be specified.
        message_id: int
            An ID referring to a message to retrieve. If this message is not in the database,
            ``channel_id`` must be specified, otherwise :obj:`None` will be returned.
        channel: discord.TextChannel
            An optional channel to look in; this is not used if ``message`` is passed, or
            ``message_id`` already exists in the database.
        cache_only: bool
            If this is :obj:`True`, the given message will only be returned if it's been
            loaded previously and is still alive in the guild's cache.
        """
        await self.load()
        if not any([message, message_id]):
            raise TypeError("neither 'message' nor 'message_id' arguments were given")

        if message_id and message_id in self._cache:
            return self._cache[message_id]
        elif message and message.id in self._cache:
            return self._cache[message.id]

        if cache_only:
            return None

        data = await Database.get_message(self.guild, message_id)
        if message_id is not None:
            # try to resolve the channel from the stored message data,
            # instead of just blindly trusting that the given channel matches;
            # this prevents messages from returning as None when they just simply exist
            # in a different channel from what we're given
            if data is not None:
                channel = shared.bot.get_channel(data.get("channel_id")) or channel

            if channel is None:
                return None

            try:
                message = await channel.fetch_message(message_id)
            except discord.HTTPException:
                return None

        if message is None:
            return None

        if message.id not in self._cache:
            star = StarboardMessage(guild=self, message=message)
            if data:
                await star.setup_from_data(data)
            self._cache[message.id] = star
        return self._cache[message.id]

    async def load(self, force=False) -> None:
        """Load config for the current guild

        If ``force`` is :obj:`False` and the current guild already has its config loaded,
        this method does nothing.
        """
        if self._config is None or force is True:
            log.debug("Loading config for guild %r", self.guild)
            data = await shared.config.guild(self.guild).all()
            # this may be left over from very old installs (read: very early 3.0.0 betas),
            # but we no longer read from this and as such it has no use being kept in memory.
            if "messages" in data:
                del data["messages"]
            self._config = data

    async def save(self) -> None:
        """Save the current guild's config"""
        if not self._config:
            return
        log.debug("Saving config for guild %r", self.guild)
        await shared.config.guild(self.guild).set(self._config)

    # noinspection PyShadowingBuiltins
    async def ignore(self, id: int) -> None:  # pylint:disable=redefined-builtin
        self.ignores.append(id)
        await self.save()

    # noinspection PyShadowingBuiltins
    async def unignore(self, id: int) -> None:  # pylint:disable=redefined-builtin
        self.ignores.remove(id)
        await self.save()

    def is_ignored(self, obj: Union[discord.Member, discord.TextChannel]) -> bool:
        return obj.id in self.ignores

    @property
    def ignores(self) -> List[int]:
        if "ignored_ids" not in self._config:
            self._config["ignored_ids"] = []
        return self._config["ignored_ids"]

    @property
    def channel(self) -> Optional[discord.TextChannel]:
        return shared.bot.get_channel(self._config.get("channel"))

    @channel.setter
    def channel(self, value: Union[discord.TextChannel, int]):
        self._config["channel"] = getattr(value, "id", value)

    @property
    def min_stars(self) -> int:
        return self._config.get("min_stars", 1)

    @min_stars.setter
    def min_stars(self, value: int):
        self._config["min_stars"] = value

    @property
    def emoji(self) -> Union[discord.Emoji, str]:
        emoji = self._config.get("emoji", DEFAULT_EMOJI)
        if not isinstance(emoji, int):
            return emoji
        return shared.bot.get_emoji(emoji) or DEFAULT_EMOJI

    @emoji.setter
    def emoji(self, value: Union[str, int]):
        self._config["emoji"] = value

    @property
    def self_star(self) -> bool:
        return self._config.get("selfstar", True)

    @self_star.setter
    def self_star(self, value: bool):
        self._config["selfstar"] = value
