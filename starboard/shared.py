import logging

from redbot.core import Config
from redbot.core.bot import Red

from red_icu import Translator

__all__ = ("config", "log", "_", "bot", "tick", "cross")
bot: Red = ...
log = logging.getLogger("red.swift_cogs.starboard")
_ = Translator("Starboard", __file__)

try:
    config = Config.get_conf(
        None, cog_name="Starboard", identifier=45351212589, force_registration=True
    )
except RuntimeError:
    # raised in headless environments such as the repl
    config = None

if config:
    config.register_global(per_worker=10)
    config.register_guild(
        **{
            "ignored_ids": [],
            "channel": None,
            "min_stars": 1,
            "selfstar": True,
            "emoji": "\N{WHITE MEDIUM STAR}",
        }
    )
    config.init_custom("MESSAGES", 2)


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


def cross(text: str) -> str:
    return f"\N{CROSS MARK} {text}"
