from redbot.core import commands
from redbot.core.commands.errors import UserFeedbackCheckFailure

from starboard import shared
from starboard.shared import _
from starboard.worker import Worker


def starboard_is_setup():
    async def predicate(ctx: commands.Context):
        if not ctx.guild:
            raise commands.NoPrivateMessage()

        starboard = Worker.get_guild(ctx.guild)
        await starboard.load()
        if starboard.channel is None:
            raise UserFeedbackCheckFailure(
                _(
                    "This server does not have a starboard channel setup; ask your server"
                    " admins to set it with `{prefix}starboard channel`."
                ).format(prefix=ctx.clean_prefix)
            )

        if starboard.is_ignored(ctx.author) and (
            not await shared.bot.is_owner(ctx.author) or await shared.bot.is_mod(ctx.author)
        ):
            raise commands.UserFeedbackCheckFailure(
                _("A server moderator has blocked you from using this server's starboard.").format()
            )
        return True

    return commands.check(predicate)
