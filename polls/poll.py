from __future__ import annotations

import logging
from asyncio import Task, sleep
from asyncio.tasks import create_task
from dataclasses import dataclass
from math import floor
from typing import Dict, List, Optional

import discord
from babel.numbers import format_percent
from red_icu import Humanize, Translator
from redbot.core.bot import Red
from redbot.core.config import Config

log = logging.getLogger("red.swift_cogs.polls")
POLLS: Dict[int, Poll] = {}
_ = Translator("Polls", __file__)
config = Config.get_conf(cog_instance=None, cog_name="Polls", identifier=4234534382)
config.init_custom("POLL", 1)


def vote_bar(percentage: float, bar_length: int = 20) -> str:
    bar = ("\N{LIGHT SHADE}", "\N{DARK SHADE}")
    filled = min(bar_length * percentage, bar_length)
    return "".join([bar[1] * floor(filled), bar[0] * floor(bar_length - filled)])


@dataclass
class Option:
    name: str
    votes: List[str]

    def to_dict(self):
        return {"name": self.name, "votes": self.votes}


@dataclass
class Poll:
    message_id: int
    channel: discord.TextChannel
    name: str
    options: List[Option]
    creator: int
    ended: bool = False

    def __post_init__(self) -> None:
        # the message ID will be None while a poll is being created
        if self.message_id is not None:
            self._message = self.channel.get_partial_message(self.message_id)
        self._update_task: Optional[Task] = None

    @classmethod
    async def create(
        cls, channel: discord.TextChannel, name: str, options: List[str], creator: discord.Member
    ) -> Poll:
        self = cls(
            message_id=None,
            channel=channel,
            name=name,
            options=[Option(name=x, votes=[]) for x in options],
            creator=creator.id,
        )
        self._message = message = await channel.send(  # pylint:disable=protected-access
            embed=self.embed
        )
        self.message_id = message.id
        for emoji in self.emojis:
            await message.add_reaction(emoji)
        await self.save()
        return self

    @classmethod
    def from_dict(cls, bot: Red, data: dict) -> Poll:
        return cls(
            options=[Option(**x) for x in data.pop("options")],
            channel=bot.get_channel(data.pop("channel_id")),
            **data,
        )

    @classmethod
    async def from_message(
        cls, bot: Red, *, message: discord.Message = None, message_id: int = None
    ) -> Optional[Poll]:
        if not message and not message_id:
            raise ValueError("Expected either message or message_id, got neither")
        mid = message and message.id or message_id
        if mid in POLLS:
            return POLLS[mid]
        data = await config.custom("POLL", mid).all()
        if not data:
            return None
        self = cls.from_dict(bot, data)
        POLLS[mid] = self
        return self

    def to_dict(self) -> dict:
        return {
            "name": self.name,
            "message_id": self.message_id,
            "channel_id": self.channel.id,
            "options": [x.to_dict() for x in self.options],
            "ended": self.ended,
            "creator": self.creator,
        }

    async def save(self) -> None:
        await config.custom("POLL", self.message_id).set(self.to_dict())

    async def save_and_update(self) -> None:
        await self.save()
        self.update()

    def update(self) -> None:
        if self._update_task and not self._update_task.done():
            log.debug("Update task already exists and isn't done")
            return
        log.debug("Queuing update on poll %r", self.name)
        self._update_task = create_task(self.__update())

    async def __update(self) -> None:
        await sleep(15)
        await self._message.edit(embed=self.embed)

    @property
    def emojis(self) -> List[str]:
        return [f"{n}\N{COMBINING ENCLOSING KEYCAP}" for n in range(1, len(self.options) + 1)]

    @property
    def total_votes(self) -> int:
        return sum(len(x.votes) for x in self.options)

    @property
    def embed(self):
        total = self.total_votes
        embed = discord.Embed(
            colour=discord.Colour.blurple() if not self.ended else discord.Colour.greyple(),
            title=self.name,
        )
        desc = []
        winning_options = [] if not self.ended else self.winning
        for option in self.options:
            pct = len(option.votes) / total if total > 0 else 0.0
            content = (
                _("{emoji} **{option}** \N{EM DASH} {percentage}")
                if option not in winning_options
                else _("{emoji} **{option}** \N{EM DASH} {percentage} \N{CROWN}")
            )
            desc.extend(
                [
                    content.format(
                        option=option.name,
                        percentage=Humanize(format_percent, pct),
                        emoji=f"{self.options.index(option) + 1}\N{COMBINING ENCLOSING KEYCAP}",
                    ),
                    vote_bar(pct),
                ]
            )
        if self.ended:
            desc = [_("*This poll has ended and can no longer be voted on.*").format(), "", *desc]
        embed.description = "\n".join(desc)
        embed.set_footer(
            text=_("{total, plural, one {# vote} other {# votes}}").format(total=total)
        )
        return embed

    @property
    def winning(self) -> List[Option]:
        winning = []
        winning_total = -1
        for option in self.options:
            if len(option.votes) > winning_total:
                winning = [option]
                winning_total = len(option.votes)
            elif len(option.votes) == winning_total:
                winning.append(option)
        return winning
