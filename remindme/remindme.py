import asyncio
import logging
from datetime import datetime, timedelta
from textwrap import shorten
from typing import Sequence, Iterable, Optional, Literal

import discord
from redbot.core import Config, commands
from redbot.core.bot import Red
from redbot.vendored.discord.ext import menus
from tzlocal import get_localzone

from swift_i18n.red import Translator
from swift_i18n import Humanize
from .reminder import Reminder

translate = Translator(__file__)
log = logging.getLogger("red.remindme")
_delta_converter = commands.get_timedelta_converter(minimum=timedelta(seconds=60))


class ListPageSource(menus.ListPageSource):
    def format_page(self, menu, page):
        return page


class ConfirmMenu(menus.Menu):
    def __init__(
        self, content: Optional[str] = None, embed: Optional[discord.Embed] = None, *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.content = content
        self.embed = embed
        self.result = None

    async def send_initial_message(self, ctx, channel):
        return await channel.send(content=self.content, embed=self.embed)

    @menus.button("\N{WHITE HEAVY CHECK MARK}")
    async def do_confirm(self, _):
        self.result = True
        self.stop()

    @menus.button("\N{CROSS MARK}")
    async def do_deny(self, _):
        self.result = False
        self.stop()

    async def prompt(self, ctx):
        await self.start(ctx, wait=True)
        return self.result


def chunks(seq: Sequence, chunk_every: int) -> Iterable[Sequence]:
    for i in range(0, len(seq), chunk_every):
        yield seq[i : i + chunk_every]


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


@translate.cog("help.cog_class")
class RemindMe(commands.Cog):
    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=2424356456, force_registration=True)
        self.config.register_user(reminders=[])
        Reminder.cfg = self.config
        Reminder.bot = self.bot
        self.task = self.bot.loop.create_task(self.reminder_handler())

    def cog_unload(self):
        self.task.cancel()

    async def red_delete_data_for_user(
        self,
        *,
        requester: Literal["discord_deleted_user", "owner", "user", "user_strict"],
        user_id: int,
    ):
        await self.config.user_from_id(user_id).clear()

    async def reminder_handler(self):
        await self.bot.wait_until_ready()
        while True:
            async for reminder in Reminder.all_reminders():
                if not reminder.is_due(True):
                    continue

                try:
                    await reminder.send_reminder()
                except discord.Forbidden:
                    log.warning(
                        "Could not send a reminder message to %r; did they block us?", reminder.user
                    )
                except discord.HTTPException as e:
                    log.exception(
                        "Failed to send reminder message to %r", reminder.user, exc_info=e
                    )
                except Exception as e:  # pylint:disable=broad-except
                    log.exception("Failed to send reminder", exc_info=e)
                    continue
                await reminder.remove()

            await asyncio.sleep(60 * 3)

    @commands.group(invoke_without_command=True, aliases=["remind", "reminder"])
    @translate.command("help.root")
    async def remindme(self, ctx: commands.Context, in_time: _delta_converter, *, to: str):
        if len(to) > 1000:
            await ctx.send(translate("content_too_long"))
            return

        in_time = in_time.total_seconds()
        reminder = await Reminder.create(ctx.author, to, in_time)
        if await ctx.embed_requested():
            embed = discord.Embed(
                colour=await ctx.embed_colour(),
                description=reminder.message,
                title=translate("reminder_created"),
                timestamp=reminder.due_on,
            )
            embed.set_footer(text=translate("will_send_on"))
            await ctx.send(embed=embed)
        else:
            await ctx.send(
                tick(
                    translate(
                        "reminder_set",
                        date=Humanize(reminder.due_on, format="long", tzinfo=get_localzone()),
                    )
                )
            )

    @remindme.command()
    @translate.command("help.clear")
    async def clear(self, ctx: commands.Context):
        if await ConfirmMenu(translate("confirm_clear")).prompt(ctx):
            await self.config.user(ctx.author).reminders.set([])
            await ctx.send(tick(translate("reminders_cleared")))
        else:
            await ctx.send(translate("ok_then"))

    @remindme.command()
    @translate.command("help.list")
    async def list(self, ctx: commands.Context):
        reminders = await self.config.user(ctx.author).reminders()
        if not reminders:
            await ctx.send(translate("no_set_reminders", prefix=ctx.prefix))
            return

        pages = list(chunks(reminders, 5))
        page_count = len(pages)
        colour = await ctx.embed_colour()
        # noinspection PyUnresolvedReferences
        for page in pages.copy():
            index = pages.index(page)
            embed = discord.Embed(colour=colour)
            embed.set_author(
                name=translate("set_reminders", user=ctx.author),
                icon_url=ctx.author.avatar_url_as(format="png"),
            )
            embed.set_footer(text=translate("page", total=page_count, current=index + 1))

            for reminder in page:
                remind_on = datetime.utcfromtimestamp(reminder["remind_on"])
                embed.add_field(
                    name=translate(
                        "reminder_title",
                        id=(index * 5) + (page.index(reminder) + 1),
                        delta=Humanize(remind_on - datetime.utcnow(), add_direction=True),
                    ),
                    value=shorten(
                        reminder["message"], width=700, placeholder=" \N{HORIZONTAL ELLIPSIS}"
                    ),
                    inline=False,
                )

            pages[index] = embed

        await menus.MenuPages(ListPageSource(pages, per_page=1), clear_reactions_after=True).start(
            ctx
        )

    @remindme.command()
    @translate.command("help.remove")
    async def remove(self, ctx: commands.Context, reminder: int):
        try:
            reminder = Reminder(
                ctx.author, (await self.config.user(ctx.author).reminders())[reminder - 1]
            )
        except IndexError:
            await ctx.send(translate("no_such_reminder", prefix=ctx.prefix))
            return

        if not await ConfirmMenu(
            content=translate("remove_confirmation"),
            embed=discord.Embed(description=reminder.message, timestamp=reminder.due_on).set_footer(
                text=translate("due_on")
            ),
        ).prompt(ctx):
            await ctx.send(translate("ok_then"))
            return

        await reminder.remove()
        await ctx.tick()
